import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../components/layout/Layout'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import("@/views/Login")
  },

  // 公共部分路由
  {
      path: '/',
      component: Layout,
      children: [
        {
          path: "",
          redirect: "/home"
        },
        {
          path: "/home",
          component: () => import("../views/Home"),
          meta: { title: '首页' ,showInbreadcrumb:true}

        },
        {
          path: "/film/list",
          component: () => import("@/views/film/List"),
          meta: { title: '影视管理 > 电影列表',showInbreadcrumb:true },

        },
        {
          path: '/film/arrange',
          component: () => import("@/views/film/Arrange"),
          meta: { title: '影视管理 > 院线排片',showInbreadcrumb:true },

        },
        {
          path: '/film/add',
          component: () => import("@/views/film/Add"),
          meta: { title: '影视管理 > 新增电影',showInbreadcrumb:true },

        },
        {
          path: '/film/poster',
          component: () => import("@/views/film/Poster"),
          meta: { title: '影视管理 > 轮播海报',showInbreadcrumb:true },

        },
        {
          path: '/user/list',
          component: () => import("@/views/user/List"),
          meta: { title: '用户管理 > 用户列表',showInbreadcrumb:true },

        },
        {
          path: '/order/list',
          component: () => import("@/views/order/List"),
          meta: { title: '订单管理 > 订单列表',showInbreadcrumb:true },

        },
        {
          path: '/order/exception',
          component: () => import("@/views/order/Exception"),
          meta: { title: '订单管理 > 异常订单',showInbreadcrumb:true },

        },
        {
          path: '/worker/list',
          component: () => import("@/views/worker/List"),
          meta: { title: '影视管理 > 员工列表',showInbreadcrumb:true },

        },
        {
          path: '/worker/daily',
          component: () => import("@/views/worker/Daily"),
          meta: { title: '员工管理 > 每日工作',showInbreadcrumb:true },

        },
        {
          path: '/worker/add',
          component: () => import("@/views/worker/Add"),
          meta: { title: '员工管理 > 新增员工',showInbreadcrumb:true },

        },
        {
          path: '/api',
          component: () => import("@/views/Api"),
          meta: { title: 'Api接口',showInbreadcrumb:true },

        },
        {
          path: '/setting',
          component: () => import("@/views/Setting"),
          meta: { title: '个人设置',showInbreadcrumb:true },

        },
        {
          path: '/403',
          component: () => import("@/views/error/403"),
          meta:{showInbreadcrumb:true }
        },
        {
          path: '/404',
          component: () => import("@/views/error/404"),
          meta: { showInbreadcrumb:true },

        }
      ]
  },

  {
    path: "*",
    redirect: '/404',
    meta:{
      showInbreadcrumb:true
    }
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//路由卫士
router.beforeEach((to, from, next) => {
  let isAuthenticated = localStorage.getItem("token") !== null
  if (to.name !== 'Login' && !isAuthenticated) next({name: 'Login'})
  else next()
})

export default router
