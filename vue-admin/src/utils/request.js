import request from "axios"
import config from "../config"
import router from '../router'
import {Notification} from 'element-ui'

// 创建axios实例
const token = localStorage.getItem("token")

const service = request.create({
  baseURL: config.API_URL,
  // 链接超时便不会发送（报错）
  timeout: 10000,
  // 头部可设置访问类型等
  headers: {'Authorization': token}
});

// response 响应拦截器
service.interceptors.response.use(
  response => {
      const res = response.data;

      // Token过期 403 跳回首页
    if (res.code === 403) {
        setTimeout( () => {
          router.push("/403")
        },0)
      response.data = null
    } else {
        if (res.success === true) {
          if (res.msg !== null) {
            // 消息通知
            // Notification.success({
            //   title: 'Success ',
            //   message: res.msg,
            //   type: 'success',
            //   duration: 600
            // });
          }
        } else {
          Notification.error({
            title: '错误消息: ' + res.code,
            message: res.msg,
            duration: 3000
          });
        }
    }
    return res
  },
  error => {
      console.log('err' + error);
      return Promise.reject(error)
  }
);

export default service
