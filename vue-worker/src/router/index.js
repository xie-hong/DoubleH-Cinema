import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import Layout from '../components/layout/Layout'
import Leaving from '../views/Leaving'
import Evaluate from '../views/worker/Evaluate'
import Activity from '../views/activity/Activity'
import Phone from '../views/worker/Active'
import Info from '../views/statistical/Info'
import Setting from '../views/Setting'
import Active from '../views/worker/Active'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },

  // 公共页面路由
  {
    path: '/',
    component: Layout,
    children: [

      {
        path: "",
        redirect: "/home"
      },
      {
        path: "/home",
        component: () => import("../views/Home"),
        meta: { title: '首页' ,showInbreadcrumb:true}

      },
      {
        path: '/leaving',
        component: Leaving,
        meta: { title: '影院留言' ,showInbreadcrumb:true}

      },
      {
        path: '/evaluate',
        component: Evaluate,
        meta: { title: '我的评价' ,showInbreadcrumb:true}

      },
      {
        path: '/activity',
        component: Activity,
        meta: { title: '活动安排' ,showInbreadcrumb:true}

      },
      {
        path: '/active',
        component: Active,
        meta: { title: '活跃排行' ,showInbreadcrumb:true}

      },
      {
        path: '/info',
        component: Info,
        meta: { title: '订单统计' ,showInbreadcrumb:true}

      },
      {
        path: '/setting',
        component: Setting,
        meta: { title: '个人设置' ,showInbreadcrumb:true}

      },
      {
        path: '/403',
        component: () => import("@/views/error/403"),
        meta:{showInbreadcrumb:true }

      },
      {
        path: '/404',
        component: () => import("@/views/error/404"),
        meta:{showInbreadcrumb:true }

      }
    ]
  },

  {
    path: "*",
    redirect: '/404',
    meta:{
      showInbreadcrumb:false
    }
  }


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//路由卫士
router.beforeEach((to, from, next) => {
  let isAuthenticated = localStorage.getItem("token") !== null
  if (to.name !== 'Login' && !isAuthenticated) next({name: 'Login'})
  else next()
})

export default router
