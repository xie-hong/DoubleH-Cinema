import service from '../request/request'

export function FindAllOrder() {
  return service({
    url: '/order',
    method: 'get'
  })
}
