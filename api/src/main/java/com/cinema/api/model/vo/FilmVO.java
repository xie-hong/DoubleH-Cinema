package com.cinema.api.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmVO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("电影ID")
    private String id;

    @ApiModelProperty("电影名称")
    private String name;

    @ApiModelProperty("上映时间")
    private String releaseTime;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("状态")
    private boolean status;

    @ApiModelProperty("地区")
    private String region;

    @ApiModelProperty("热度")
    private Integer hot;

    @ApiModelProperty("电影简介")
    private String introduction;

    @ApiModelProperty("时长")
    private String cover;

    @ApiModelProperty("封面地址")
    private Integer duration;

    @ApiModelProperty("创建时间")
    private String createTime;

    @ApiModelProperty("更新时间")
    private String updateTime;

    @ApiModelProperty("放映类型")
    private String showType;

    @ApiModelProperty("电影票房")
    private String boxOffice;

}
