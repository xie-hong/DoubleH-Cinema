package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_worker")
@ApiModel(value = "Worker对象", description = "")
public class Worker implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("员工客服ID")
    @TableId("id")
    private String id;

    @ApiModelProperty("员工客服名")
    @TableField("username")
    private String username;

    @ApiModelProperty("密码")
    @TableField("password")
    private String password;

    @ApiModelProperty("昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("电话")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("入职")
    @TableField("entry")
    private Boolean entry;

    @ApiModelProperty("部门")
    @TableField("department")
    private String department;

    @ApiModelProperty("性别")
    @TableField("gender")
    private String gender;

    @ApiModelProperty("头像")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty("逻辑删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
