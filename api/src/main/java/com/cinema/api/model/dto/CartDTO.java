package com.cinema.api.model.dto;

import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.Cart;
import com.cinema.api.model.entity.Film;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO implements Serializable {

    /**
     * 该订单电影信息
     */
    private Film film;

    /**
     * 该订单排片信息
     */
    private Arrangement arrangement;

    /**
     * 该用户单个订单
     */
    private Cart cart;
}
