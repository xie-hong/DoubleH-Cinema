package com.cinema.api.model.vo;

import lombok.Data;

/**
 * @author 谢泓泓
 */
@Data
public class LoginVO {
    private String username;
    private String password;
    /**
     * 记住我
     */
    private boolean remember;
}
