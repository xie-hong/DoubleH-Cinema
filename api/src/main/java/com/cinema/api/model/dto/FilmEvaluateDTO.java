package com.cinema.api.model.dto;

import com.cinema.api.model.entity.FilmEvaluate;
import com.cinema.api.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmEvaluateDTO implements Serializable {

    private String id;

    private FilmEvaluate filmEvaluate;

    private User user;
}

