package com.cinema.api.model.enums;

/**
 * 错误返回接口类
 */
public interface ResultInfoInterface {

    Integer getResultCode();

    String getResultMsg();

}
