package com.cinema.api.model.dto;

import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.Film;
import com.cinema.api.model.entity.Order;
import com.cinema.api.model.entity.User;
import lombok.Data;

/**
 * @author 谢泓泓
 */
@Data
public class OrderDTO {
    private String expireTime;

    private Order order;

    private User user;

    private Film film;

    private Arrangement arrangement;
}
