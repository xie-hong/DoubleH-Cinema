package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_film_evaluate")
@ApiModel(value = "FilmEvaluate对象", description = "")
public class FilmEvaluate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("评价id")
    @TableId("id")
    private String id;

    @ApiModelProperty("电影id")
    @TableField("fid")
    private String fid;

    @ApiModelProperty("用户id")
    @TableField("uid")
    private String uid;

    @ApiModelProperty("星级")
    @TableField("star")
    private Integer star;

    @ApiModelProperty("评语")
    @TableField("comment")
    private String comment;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("逻辑删除：0，1")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
