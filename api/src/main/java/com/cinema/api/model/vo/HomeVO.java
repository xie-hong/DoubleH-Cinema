package com.cinema.api.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomeVO {

    @ApiModelProperty("用户数量")
    private Integer user;
    @ApiModelProperty("电影数量")
    private Integer film;
    @ApiModelProperty("排片场次")
    private Integer arrangement;
    @ApiModelProperty("影院票房")
    private Integer boxOffice;
    @ApiModelProperty("留言数")
    private Integer leaving;
    @ApiModelProperty("个人评价")
    private Integer evaluate;
    @ApiModelProperty("订单总数")
    private Integer order;


}
