package com.cinema.api.model.dto;

import com.cinema.api.model.entity.User;
import com.cinema.api.model.entity.WorkerEvaluate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkerEvaluateDTO implements Serializable {

    private User user;

    private WorkerEvaluate workerEvaluate;
}
