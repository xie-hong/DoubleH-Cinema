package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_activity")
@ApiModel(description = "影院活动")
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @ApiModelProperty("活动标题")
    @TableField("title")
    private String title;

    @ApiModelProperty("活动内容")
    @TableField("content")
    private String content;

    @ApiModelProperty("活动参与人数")
    @TableField("number")
    private Integer number;

    @ApiModelProperty("开始时间")
    @TableField("start_time")
    private String startTime;

    @ApiModelProperty("结束时间")
    @TableField("end_time")
    private String endTime;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("逻辑删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
