package com.cinema.api.model.dto;

import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.Film;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArrangementDTO implements Serializable {

    /**
     * 当前电影所有排片信息
     */
    private List<Arrangement> arrangementList;

    /**
     * 当前电影信息
     */
    private Film film;

}
