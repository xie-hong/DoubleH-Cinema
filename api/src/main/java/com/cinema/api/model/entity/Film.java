package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_film")
@ApiModel(value = "Film对象", description = "")
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("电影ID")
    @TableId("id")
    private String id;

    @ApiModelProperty("电影名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("上映时间")
    @TableField("release_time")
    private String releaseTime;

    @ApiModelProperty("类型")
    @TableField("type")
    private String type;

    @ApiModelProperty("状态")
    @TableField("status")
    private boolean status;

    @ApiModelProperty("地区")
    @TableField("region")
    private String region;

    @ApiModelProperty("热度")
    @TableField("hot")
    private Integer hot;

    @ApiModelProperty("电影简介")
    @TableField("introduction")
    private String introduction;

    @ApiModelProperty("时长")
    @TableField("cover")
    private String cover;

    @ApiModelProperty("封面地址")
    @TableField("duration")
    private Integer duration;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty("放映类型")
    @TableField("show_type")
    private String showType;

    @ApiModelProperty("逻辑删除：0，1")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
