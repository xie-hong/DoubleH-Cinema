package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_arrangement")
@ApiModel(value = "Arrangement对象", description = "")
public class Arrangement implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("排片ID")
    @TableId("id")
    private String id;

    @ApiModelProperty("电影ID")
    @TableField("fid")
    private String fid;

    @ApiModelProperty("电影名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("座位数量")
    @TableField("seat_number")
    private Integer seatNumber;

    @ApiModelProperty("票房统计")
    @TableField("box_office")
    private String boxOffice;

    @ApiModelProperty("价格")
    @TableField("price")
    private BigDecimal price;

    @ApiModelProperty("放映类型")
    @TableField("type")
    private String type;

    @ApiModelProperty("播放日期")
    @TableField("date")
    private String date;

    @ApiModelProperty("具体开始时间")
    @TableField("start_time")
    private String startTime;

    @ApiModelProperty("具体结束时间")
    @TableField("end_time")
    private String endTime;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty("创建人")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty("逻辑删除：0，1")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
