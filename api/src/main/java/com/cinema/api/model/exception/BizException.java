package com.cinema.api.model.exception;

import com.cinema.api.model.enums.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义异常处理类（处理发生的业务异常 可自定义）
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "自定义异常处理类")
public class BizException extends RuntimeException {

    private Integer errorCode;
    private String errorMsg;

    public BizException(ResultCodeEnum resultCodeEnum) {
        this.errorMsg = resultCodeEnum.getMsg();
        this.errorCode = resultCodeEnum.getCode();
    }

}
