package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_order_exception")
@ApiModel(value = "OrderException对象", description = "")
public class OrderException implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("异常ID")
    @TableId("id")
    private String id;

    @ApiModelProperty("订单ID")
    @TableField("oid")
    private String oid;

    @ApiModelProperty("异常原因")
    @TableField("reason")
    private String reason;

    @ApiModelProperty("处理状态")
    @TableField("status")
    private Boolean status;

    @TableField("result")
    private String result;

    @ApiModelProperty("审核人员")
    @TableField("reviewer")
    private String reviewer;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("结束时间")
    @TableField("end_time")
    private String endTime;

    @ApiModelProperty("逻辑删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
