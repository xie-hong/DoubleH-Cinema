package com.cinema.api.model.dto;

import com.cinema.api.model.entity.LeavingMessage;
import com.cinema.api.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeavingMessageDTO implements Serializable {

    private String id;

    private LeavingMessage leavingMessage;

    private User user;
}
