package com.cinema.api.model.dto;

import com.cinema.api.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 谢泓泓
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActiveUserDTO implements Serializable {

    private User user;

    /**
     * 留言次数
     */
    private Integer number;
}
