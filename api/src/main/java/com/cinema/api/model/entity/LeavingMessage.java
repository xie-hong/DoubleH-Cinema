package com.cinema.api.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Getter
@Setter
@TableName("t_leaving_message")
@ApiModel(value = "LeavingMessage对象", description = "")
public class LeavingMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("留言ID")
    @TableId("id")
    private String id;

    @ApiModelProperty("用户ID")
    @TableField("uid")
    private String uid;

    @ApiModelProperty("内容")
    @TableField("content")
    private String content;

    @ApiModelProperty("回复")
    @TableField("reply")
    private String reply;

    @ApiModelProperty("回复人")
    @TableField("replyBy")
    private String replyBy;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty("逻辑删除")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;


}
