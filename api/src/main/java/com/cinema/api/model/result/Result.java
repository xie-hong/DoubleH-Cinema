package com.cinema.api.model.result;

import com.cinema.api.model.enums.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 统一的数据格式返回
 * @param <T>
 */
@Data
@AllArgsConstructor
@ApiModel(value = "统一结果返回类")
public class Result<T> implements Serializable {
    private Integer code;
    private String msg;
    private T data;
    private boolean success;//响应结果是否成功

    /**
     * 构造器私有
     */
    private Result() {
    }
    /**
     * 通用 成功返回
     */
    public static Result success() {
        Result result = new Result();
        result.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMsg(ResultCodeEnum.SUCCESS.getMsg());
        result.setData(null);
        return result;
    }
    /**
     * 成功返回数据
     * @param data
     * @return
     */
    public static <T> Result<T> success(T data) {
        Result result = new Result();
        result.setSuccess(ResultCodeEnum.SUCCESS.getSuccess());
        result.setCode(ResultCodeEnum.SUCCESS.getCode());
        result.setMsg(ResultCodeEnum.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }
    /**
     * 通用 失败返回、未知错误
     */
    public static Result error() {
        Result result = new Result();
        result.setSuccess(ResultCodeEnum.UNKNOWN_ERROR.getSuccess());
        result.setCode(ResultCodeEnum.UNKNOWN_ERROR.getCode());
        result.setMsg(ResultCodeEnum.UNKNOWN_ERROR.getMsg());
        result.setData(null);
        return result;
    }
    /**
     * 自定义 失败返回
     * @param code
     * @param msg
     * @return
     */
    public static Result error(Integer code, String msg) {
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        result.setData(null);
        return result;
    }
    /**
     * 自定义 失败返回
     * @param msg
     * @return
     */
    public static Result error(String msg) {
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(400);
        result.setMsg(msg);
        result.setData(null);
        return result;
    }
    /**
     * 设置结果返回
     * @param resultCodeEnum
     * @return
     */
    public static Result setResult(ResultCodeEnum resultCodeEnum) {
        Result result = new Result();
        result.setSuccess(resultCodeEnum.getSuccess());
        result.setCode(resultCodeEnum.getCode());
        result.setMsg(resultCodeEnum.getMsg());
        result.setData(null);
        return result;
    }

}
