package com.cinema.api.model.enums;

import lombok.Getter;

/**
 * 状态码 枚举类
 * @author 谢泓泓
 */
@Getter
public enum ResultCodeEnum implements ResultInfoInterface {

    // 基本集合
    SUCCESS(true,20000,"成功"),
    UNKNOWN_ERROR(false,20001,"未知错误"),
    PARAM_ERROR(false,20002,"请求参数错误"),
    NULL_POINT(false,20003,"空指针异常"),
    HTTP_CLIENT_ERROR(false,20004,"客户端连接异常"),
    NOT_FOUND(false,20005, "未找到该资源!"),
    USER_NOT_LOGIN(false,20006,"用户还未登录,请先登录"),
    NO_PERMISSION(false,403,"权限不足,请联系管理员"),
    SYS_REQUEST_METHOD_NOT_SUPPORTED(false, 20008, "不支持的请求方法"),
    USERNAME_PASSWORD_NOT_FOUND(false, 20009, "用户名或密码错误"),
    // 数据管理
    ADD_UPDATE_IS_EXIT(false, 1001, "已存在"),
    DATA_IS_EMPTY(false, 1002, "数据为空！"),
    // 用户端错误
    USER__EXCEPTION(false, 2000, "用户端错误"),
    USER_FILM_EXCEPTION(false, 2001, "电影类型错误"),
    USER_ACTIVITY_EXCEPTION(false, 2002, "活动类型错误"),
    USER_ORDER_EXCEPTION(false, 2003, "订单类型错误"),

    /**
     * 20041：离职
     * 20043：权限类错误
     */
    USER_WORKER_EXCEPTION(false, 20040, "用户员工异常"),

    ;

    /**
     * 响应是否成功
     */
    private Boolean success;
    /**
     * 响应状态码
     */
    private Integer code;
    /**
     * 响应信息
     */
    private String msg;

    ResultCodeEnum(boolean success, Integer code, String msg) {
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    @Override
    public Integer getResultCode() {
        return code;
    }

    @Override
    public String getResultMsg() {
        return msg;
    }

}
