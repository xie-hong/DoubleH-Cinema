package com.cinema.api.mapper;

import com.cinema.api.model.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    String getPrices(String id);
}
