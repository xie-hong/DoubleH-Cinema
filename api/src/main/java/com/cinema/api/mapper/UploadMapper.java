package com.cinema.api.mapper;

import com.cinema.api.model.entity.Upload;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface UploadMapper extends BaseMapper<Upload> {

}
