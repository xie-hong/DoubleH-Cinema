package com.cinema.api.mapper;

import com.cinema.api.model.entity.Film;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cinema.api.model.vo.FilmVO;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface FilmMapper extends BaseMapper<Film> {

    String selectByEditName(String name, String id);

    List<Film> select(String status);

    List<FilmVO> findAll();

    List<FilmVO> findByRegionAndType(String region, String type);

    List<FilmVO> findByStatus(String status, String release);


    List<FilmVO> boxOfficeList(Integer limit);

    List<FilmVO> findByLike(String name);

}
