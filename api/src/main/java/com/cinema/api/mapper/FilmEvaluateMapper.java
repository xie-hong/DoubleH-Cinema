package com.cinema.api.mapper;

import com.cinema.api.model.entity.FilmEvaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface FilmEvaluateMapper extends BaseMapper<FilmEvaluate> {

}
