package com.cinema.api.mapper;

import com.cinema.api.model.entity.Arrangement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cinema.api.model.entity.Film;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface ArrangementMapper extends BaseMapper<Arrangement> {

    void UpdateByPrice(String fid, String price);

    Arrangement findOneByFid(String fid);

    Integer getOffice();

    List<Arrangement> findByLike(String name);
}
