package com.cinema.api.mapper;

import com.cinema.api.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> findByLike(String username);
}
