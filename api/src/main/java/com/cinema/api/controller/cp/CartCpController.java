package com.cinema.api.controller.cp;


import com.cinema.api.model.dto.CartDTO;
import com.cinema.api.model.entity.Cart;
import com.cinema.api.service.CartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api(tags = "用户端 - 购物车接口")
@RequestMapping("/api/cp/cart")
public class CartCpController {

    @Resource
    private CartService cartService;

    @PostMapping
    @ApiOperation("添加购物车")
    public void add(@RequestBody Cart cart) {
        cartService.add(cart);
    }

    @GetMapping("")
    @ApiOperation("根据用户id查询购物车")
    public List<CartDTO> findAllByUid(@RequestParam String uid) {
        return cartService.findAllByUid(uid);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据Id删除购物车")
    public void deleteById(@PathVariable String id) {
        cartService.deleteById(id);
    }
}
