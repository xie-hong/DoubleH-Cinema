package com.cinema.api.controller;


import com.cinema.api.model.entity.Activity;
import com.cinema.api.model.enums.ResultCodeEnum;
import com.cinema.api.model.exception.BizException;
import com.cinema.api.model.result.Result;
import com.cinema.api.service.ActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api( tags = "影院活动接口")
@RequestMapping("/api/activity")
public class ActivityController {
    @Resource
    private ActivityService activityService;

    @GetMapping("")
    @ApiOperation("获取全部活动")
    public List<Activity> findAll() {
        return activityService.findAll();
    }

    @PostMapping("")
    @ApiOperation("新增或更新活动")
    public void add(@RequestBody Activity activity) {
        activityService.add(activity);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查找活动")
    public Activity findById(@PathVariable String id) {
        return activityService.findById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据ID删除活动")
    public void deleteById(@PathVariable String id) {
        activityService.deleteById(id);
    }
}
