package com.cinema.api.controller;

import com.cinema.api.model.vo.HomeVO;
import com.cinema.api.service.HomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 谢泓泓
 */
@Slf4j
@Api(tags = "首页展示接口")
@RestController
@RequestMapping("/api/home")
public class HomeController {
    @Resource
    private HomeService homeService;

    @GetMapping("")
    @ApiOperation("首页信息展示")
    public HomeVO findHome() {
        return homeService.findHome();
    }
}
