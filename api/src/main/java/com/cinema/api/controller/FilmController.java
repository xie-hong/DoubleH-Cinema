package com.cinema.api.controller;


import com.cinema.api.model.entity.Film;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.service.FilmService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "电影接口")
@RestController
@RequestMapping("/api/film")
public class FilmController {

    @Resource
    private FilmService filmService;

    @GetMapping("/page")
    @ApiOperation("电影列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value="页码", paramType = "query" ,required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value="每页条数", paramType = "query",required = true, defaultValue = "10"),
    })
    public PageInfo<FilmVO> filmList(@RequestParam int page, @RequestParam int limit) {
        return filmService.findAllByPage(page, limit);
    }

    @GetMapping("")
    @ApiOperation("电影列表")
    public List<FilmVO> filmList(){
        return filmService.findAll();
    }

    @RequestMapping("/like")
    @ApiOperation("模糊查询电影列表")
    public PageInfo<FilmVO> filmLikeList(@RequestParam Map<String, Object> params){
        int page = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        String name = (String) params.get("name");

        return filmService.findByLike(page, limit, name);
    }

    @PostMapping("")
    @PreAuthorize("hasAnyRole('ROLE_ADD_FILE')")
    @ApiOperation(value = "新增或更新编辑电影")
    public void saveOrEdit(@RequestBody Film film) {
        filmService.saveOrEdit(film);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询电影信息")
    public Film findById(@PathVariable String id) throws ParseException {
        return filmService.findById(id);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_DELETE_FILE')")
    @ApiOperation(value = "根据Id删除电影")
    public void deleteById(@PathVariable String id) {
        filmService.deleteById(id);
    }


    @GetMapping("/name/{name}")
    @ApiOperation("搜索名称模糊查询电影")
    public List<Film> findByName(@PathVariable String name) {
        return filmService.findByName(name);
    }

}
