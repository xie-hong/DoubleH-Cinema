package com.cinema.api.controller;


import com.cinema.api.annotation.DisableBaseResponse;
import com.cinema.api.mapper.UploadMapper;
import com.cinema.api.model.entity.Upload;
import com.cinema.api.model.enums.ResultCodeEnum;
import com.cinema.api.model.exception.BizException;
import com.cinema.api.service.UploadService;
import com.cinema.api.utils.BaseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "上传接口")
@RestController
@RequestMapping("/api/upload")
public class UploadController {

    @Value("${server.port}")
    private String serverPort;

    @Value("${name.address}")
    private String address;

    @Resource
    private UploadMapper uploadMapper;

    @Resource
    private UploadService uploadService;

    @PostMapping("")
    @ApiOperation(value = "图片上传")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_WORKER')")
    @DisableBaseResponse
    public String upload(MultipartFile file) {
        if (file == null) {
            throw new BizException(ResultCodeEnum.NOT_FOUND);
        }
        if (file.isEmpty()) {
            throw new BizException(21004,"上传失败，请选择文件");
        }
        return "http://"+ address + ":" + serverPort + "/api/upload?id=" + uploadService.checkAndSaveUpload(file);
    }

    @GetMapping("")
    @ApiOperation(value = "获取图片")
    @PermitAll
    @DisableBaseResponse
    public void get(@RequestParam("id") String id, HttpServletResponse response) throws Exception {
        if ("".equals(id)) {
            return;
        }
        // 根据ID获取对应图片文件的值
        Upload upload = uploadMapper.selectById(id);
        if (BaseUtil.isEmpty(upload)) {
            throw new BizException(ResultCodeEnum.NOT_FOUND);
        }
        byte[] data = upload.getBytes();
        response.setContentType("image/jpeg");
        response.setCharacterEncoding("UTF-8");
        OutputStream outputStream = response.getOutputStream();
        InputStream in = new ByteArrayInputStream(data);
        int len;
        byte[] buf = new byte[1024];
        while ((len = in.read(buf, 0, 1024)) != -1) {
            outputStream.write(buf, 0, len);
        }
        outputStream.close();
    }

}
