package com.cinema.api.controller.cp;


import com.cinema.api.model.dto.ArrangementDTO;
import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.Film;
import com.cinema.api.service.ArrangementService;
import com.cinema.api.service.FilmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "用户端-电影排片接口")
@RestController
@RequestMapping("/api/cp/arrangement")
public class ArrangementCpController {

    @Resource
    private ArrangementService arrangementService;

    @Resource
    private FilmService filmService;

    @GetMapping("")
    @ApiOperation("根据fid查询对应电影的所有排片信息")
    public ArrangementDTO findByFid(@RequestParam String fid) {
        return arrangementService.findByFid(fid);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据排片id 查询对应场次排片")
    public Map<String, Object> findById(@PathVariable String id) throws ParseException {
        HashMap<String, Object> map = new HashMap<>();
        Arrangement arrangement = arrangementService.findById(id);
        Film film = filmService.findById(arrangement.getFid());
        map.put("film", film);
        map.put("arrangement", arrangement);
        return map;
    }

    @GetMapping("/getSeats")
    @ApiOperation("获取该场次已被选择订购的座位集合")
    public List<Integer> getSeatsHaveSelect(@RequestParam String id) {
        return arrangementService.getSeatsHaveSelect(id);
    }
}
