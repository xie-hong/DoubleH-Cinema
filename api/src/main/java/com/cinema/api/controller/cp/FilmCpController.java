package com.cinema.api.controller.cp;


import com.cinema.api.model.entity.Film;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.service.FilmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "用户端 电影接口")
@RestController
@RequestMapping("/api/cp/film")
public class FilmCpController {

    @Resource
    private FilmService filmService;

    @GetMapping("")
    @ApiOperation("电影列表")
    public List<FilmVO> filmList(String region, String type, String status, String release) throws Exception{
        if (status != null) {
            // 返回已上映的热播电影
            return filmService.findByStatus(status, release);
        }
        if (region != null && type != null) {
            return filmService.findByRegionAndType(region, type);
        }
        return filmService.findAll();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据id查询电影信息")
    public Film findById(@PathVariable String id) throws ParseException {
        return filmService.findById(id);
    }

    @GetMapping("/name/{name}")
    @ApiOperation("搜索名称模糊查询电影")
    public List<Film> findByName(@PathVariable String name) {
        return filmService.findByName(name);
    }

    @GetMapping("/hot/{limit}")
    @ApiOperation("获取热榜前10的电影")
    public List<Film> hotList(@PathVariable Integer limit) throws ParseException {
        return filmService.hotList(limit);
    }

    @GetMapping("/boxOffice/{limit}")
    @ApiOperation("获取票房前10的电影")
    public List<FilmVO> boxOfficeList(@PathVariable Integer limit) throws ParseException {
        return filmService.boxOfficeList(limit);
    }
}
