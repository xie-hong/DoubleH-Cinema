package com.cinema.api.controller;


import com.cinema.api.constant.Roles;
import com.cinema.api.model.entity.Role;
import com.cinema.api.service.RoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @GetMapping("/system")
    @ApiOperation("查看并返回系统设置的权限")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String[] findSystemRoles() {
        return Roles.ROLES;
    }

    @GetMapping("")
    @ApiOperation("根据ID查询员工的权限")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<Role> findRoleListById(@RequestParam String wid) {
        return roleService.findRoleListById(wid);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据ID删除权限")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void deleteById(@PathVariable String id) throws Exception {
        roleService.deleteById(id);
    }

    @PostMapping("")
    @ApiOperation("为员工添加权限")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public void addRole(@RequestBody Role role) throws Exception{
        roleService.add(role);
    }
}
