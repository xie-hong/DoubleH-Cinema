package com.cinema.api.controller.cp;


import com.cinema.api.mapper.FilmEvaluateMapper;
import com.cinema.api.model.dto.FilmEvaluateDTO;
import com.cinema.api.model.entity.Film;
import com.cinema.api.model.entity.FilmEvaluate;
import com.cinema.api.service.FilmEvaluateService;
import com.cinema.api.utils.BaseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api(tags = "用户端-电影评价接口")
@RequestMapping("/api/cp/fe")
public class FilmEvaluateCpController {

    @Resource
    private FilmEvaluateService filmEvaluateService;

    @PostMapping("")
    @ApiOperation("新增电影评论")
    public void add(@RequestBody FilmEvaluate filmEvaluate) throws Exception {
        filmEvaluateService.add(filmEvaluate);
    }

    @GetMapping("")
    @ApiOperation("获取(根据Fid)电影评论")
    public List<FilmEvaluateDTO> findByFid(@RequestParam("fid") String fid) {
        if (BaseUtil.isNotEmpty(fid)) {
            // 返回全部评论
        }
        return filmEvaluateService.findByFid(fid);
    }
}
