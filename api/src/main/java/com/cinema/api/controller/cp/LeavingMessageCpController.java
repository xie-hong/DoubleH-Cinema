package com.cinema.api.controller.cp;


import com.cinema.api.model.dto.ActiveUserDTO;
import com.cinema.api.model.dto.LeavingMessageDTO;
import com.cinema.api.model.entity.LeavingMessage;
import com.cinema.api.service.LeavingMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api( tags = "用户端 - 留言接口")
@RequestMapping("/api/cp/lm")
public class LeavingMessageCpController {

    @Resource
    private LeavingMessageService leavingMessageService;

    @GetMapping("")
    @ApiOperation("获取所有影院留言")
    public List<LeavingMessageDTO> findAll() {
        return leavingMessageService.findAll();
    }

    @PostMapping("")
    @ApiOperation(value = "新增或回复（更新）留言")
    public void addOrReply(@RequestBody LeavingMessage leavingMessage) {
        leavingMessageService.addOrReply(leavingMessage);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "根据ID删除留言")
    public void deleteById(@PathVariable String id) {
        leavingMessageService.deleteById(id);
    }

}
