package com.cinema.api.controller.cp;


import com.cinema.api.model.entity.Poster;
import com.cinema.api.service.PosterService;
import com.cinema.api.utils.BaseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "用户端 首页轮播海报接口")
@RestController
@RequestMapping("/api/cp/poster")
public class PosterCpController {

    @Resource
    private PosterService posterService;

    @GetMapping("")
    @ApiOperation("获取所有海报")
    public List<Poster> posterList(Boolean status) {
        if (BaseUtil.isNotEmpty(status)) {
            // 用户端获取 上架海报
            return posterService.findByStatus(status);
        }
        return posterService.findAll();
    }


}
