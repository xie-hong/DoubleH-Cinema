package com.cinema.api.controller;


import com.cinema.api.model.dto.WorkerEvaluateDTO;
import com.cinema.api.model.entity.WorkerEvaluate;
import com.cinema.api.service.WorkerEvaluateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@RequestMapping("/api/we")
public class WorkerEvaluateController {

    @Resource
    private WorkerEvaluateService workerEvaluateService;

    @GetMapping("")
    @ApiOperation(value = "根据员工Id查看客服评价")
    public List<WorkerEvaluateDTO> findById(@RequestParam String wid) {
        return workerEvaluateService.findById(wid);
    }
}
