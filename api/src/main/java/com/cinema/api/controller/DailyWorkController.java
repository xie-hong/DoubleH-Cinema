package com.cinema.api.controller;


import com.cinema.api.model.entity.DailyWork;
import com.cinema.api.service.DailyWorkService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "日常工作目标接口")
@RestController
@RequestMapping("/api/daily")
public class DailyWorkController {

    @Resource
    private DailyWorkService dailyWorkService;

    @GetMapping("")
    @ApiOperation("查询所有每日工作")
    public List<DailyWork> findAll() {
        return dailyWorkService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询所有每日工作")
    public PageInfo<DailyWork> findAllByPage(@RequestParam int page, @RequestParam int limit) {
        return dailyWorkService.findAllByPage(page, limit);
    }

    @PostMapping("")
    @ApiOperation("添加每日工作目标")
    public void add(@RequestBody DailyWork dailyWork) {
        dailyWorkService.add(dailyWork);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据id删除每日工作目标")
    public void deleteById(@PathVariable String id) {
        dailyWorkService.deleteById(id);
    }
}
