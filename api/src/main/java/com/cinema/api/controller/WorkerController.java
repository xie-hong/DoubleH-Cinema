package com.cinema.api.controller;


import com.cinema.api.constant.Roles;
import com.cinema.api.model.entity.Role;
import com.cinema.api.model.entity.Worker;
import com.cinema.api.model.vo.LoginVO;
import com.cinema.api.service.RoleService;
import com.cinema.api.service.WorkerService;
import com.cinema.api.utils.JwtUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "员工接口")
@RestController
@RequestMapping("/api/worker")
public class WorkerController {

    @Resource
    private WorkerService workerService;

    @Resource
    private RoleService roleService;

    @PostMapping("/login")
    @ApiOperation("员工登录")
    public Map<String, Object> login(@RequestBody LoginVO loginVO) throws Exception {
        Map<String, Object> map = new HashMap<>();
        // 校验并获取员工信息
        Worker worker = workerService.login(loginVO);
        // 是否记住我
        long exp = loginVO.isRemember() ? JwtUtil.REMEMBER_EXPIRATION_TIME : JwtUtil.EXPIRATION_TIME;
        List<String> roles = new ArrayList<>();
        // 添加员工客服基本权限
        roles.add(Roles.ROLE_WORKER);
        // 在权限表中查询登录的员工客服有哪些权限，并添加
        for (Role role : roleService.findRoleListById(worker.getId())) {
            roles.add(role.getValue());
        }
        map.put("token", JwtUtil.createToken(loginVO.getUsername(), roles, exp));
        map.put("worker", worker);
        return map;
    }

    @GetMapping("")
    @ApiOperation("查询全部员工")
    public List<Worker> findAll() {
        return workerService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询全部员工")
    public PageInfo<Worker> findAllByPage(@RequestParam int page, @RequestParam int limit) {
        return workerService.findAllByPage(page, limit);
    }

    @GetMapping("/like")
    @ApiOperation("根据员工名模糊分页查询全部员工")
    public PageInfo<Worker> filmLikeList(@RequestParam Map<String, Object> params) {
        int page = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        String username = (String) params.get("username");
        return workerService.filmLikeList(page, limit, username);
    }

    @PostMapping("")
    @ApiOperation("添加或更新员工信息")
    public void create(@RequestBody Worker worker) throws Exception {
        workerService.createOrUpdate(worker);
    }

    // @PutMapping("")
    // @ApiOperation("更新员工信息")
    // public void update(@RequestBody Worker worker) throws Exception {
    //     workerService.createOrUpdate(worker);
    // }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询员工信息")
    public Worker findById(@PathVariable String id) {
        return workerService.findById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据id删除员工以及权限信息")
    public void deleteById(@PathVariable String id){
        workerService.deleteById(id);
    }
}
