package com.cinema.api.controller;


import com.cinema.api.model.dto.ActiveUserDTO;
import com.cinema.api.model.dto.LeavingMessageDTO;
import com.cinema.api.model.entity.LeavingMessage;
import com.cinema.api.service.LeavingMessageService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@RequestMapping("/api/lm")
public class LeavingMessageController {

    @Resource
    private LeavingMessageService leavingMessageService;

    @GetMapping("")
    @ApiOperation("获取所有影院留言")
    public List<LeavingMessageDTO> findAll() {
        return leavingMessageService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation("分页获取所有影院留言")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value="页码", paramType = "query" ,required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value="每页条数", paramType = "query",required = true, defaultValue = "10"),
    })
    public PageInfo<LeavingMessageDTO> findAllByPage(@RequestParam int page, @RequestParam int limit) {
        return leavingMessageService.findAllByPage(page, limit);
    }

    @PostMapping("")
    @ApiOperation(value = "新增或回复（更新）留言")
    public void addOrReply(@RequestBody LeavingMessage leavingMessage) {
        leavingMessageService.addOrReply(leavingMessage);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "根据ID删除留言")
    public void deleteById(@PathVariable String id) {
        leavingMessageService.deleteById(id);
    }

    @GetMapping("/active")
    @ApiOperation("获取活跃留言的用户")
    public List<ActiveUserDTO> findActiveUser() {
        return leavingMessageService.findActiveUser();
    }

}
