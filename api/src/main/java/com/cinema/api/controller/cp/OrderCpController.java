package com.cinema.api.controller.cp;

import com.cinema.api.model.dto.OrderDTO;
import com.cinema.api.model.entity.Cart;
import com.cinema.api.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;

/**
 * @author 谢泓泓
 */
@RestController
@Api(tags = "用户端 - 订单接口")
@RequestMapping("/api/cp/order")
public class OrderCpController {

    @Resource
    private OrderService orderService;

    @PostMapping("")
    @ApiOperation(value = "创建订单")
    public void create(@RequestBody Cart cart) {
        orderService.create(cart);
    }

    @GetMapping("/user/{uid}")
    @ApiOperation(value = "根据用户Id查询用户订单")
    public List<OrderDTO> findByUid(@PathVariable String uid) throws ParseException {
        return orderService.findByUid(uid);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据Id删除用户订单")
    public void deleteById(@PathVariable String id) {
        orderService.deleteById(id);
    }

    @PostMapping("/pay")
    @ApiOperation(value = "根据订单Id 支付订单 更改状态")
    public void payById(@RequestParam String id) {
        orderService.payById(id);
    }
}
