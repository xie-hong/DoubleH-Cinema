package com.cinema.api.controller;


import com.cinema.api.model.entity.Activity;
import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.service.ArrangementService;
import com.cinema.api.service.FilmService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "电影排片接口")
@RestController
@RequestMapping("/api/arrangement")
public class ArrangementController {

    @Resource
    private ArrangementService arrangementService;

    @Resource
    private FilmService filmService;

    @GetMapping("")
    @ApiOperation("查询所有电影排片")
    public List<Arrangement> findAll() {
        return arrangementService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询所有电影排片")
    public PageInfo<Arrangement> findAllByPage(@RequestParam int page, @RequestParam int limit) {
        return arrangementService.findAllByPage(page, limit);
    }

    @GetMapping("/like")
    @ApiOperation("分页查询所有电影排片")
    public PageInfo<Arrangement> findAllByPage(@RequestParam Map<String, Object> params) {
        int page = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        String name = (String) params.get("name");
        return arrangementService.findAllLike(page, limit, name);
    }

    @PostMapping("")
    @PreAuthorize("hasAnyRole('ROLE_ADD_ARRANGEMENT')")
    @ApiOperation("新增或修改电影场次")
    public void addOrUpdate(@RequestBody Arrangement arrangement) {
        arrangementService.addOrUpdate(arrangement);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查找排片")
    public Arrangement findById(@PathVariable String id) {
        return arrangementService.findById(id);
    }


    @PostMapping("/price")
    @ApiOperation("更新排片票房")
    public void UpdateByPrice(@RequestParam String fid, @RequestParam String price) {
        arrangementService.UpdateByPrice(fid, price);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_DELETE_ARRANGEMENT')")
    @ApiOperation("根据id删除排片")
    public void deleteById(@PathVariable String id) {
        arrangementService.deleteById(id);
    }

}
