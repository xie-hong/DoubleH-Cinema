package com.cinema.api.controller.cp;


import com.cinema.api.model.entity.Activity;
import com.cinema.api.service.ActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api( tags = "用户端 - 影院活动接口")
@RequestMapping("/api/cp/activity")
public class ActivityCpController {
    @Resource
    private ActivityService activityService;

    @GetMapping("")
    @ApiOperation("用户端 获取全部活动")
    public List<Activity> findAllForUser() {
        return activityService.findAllForUser();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查找活动")
    public Activity findById(@PathVariable String id) {
        return activityService.findById(id);
    }

   }
