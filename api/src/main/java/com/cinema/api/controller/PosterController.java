package com.cinema.api.controller;


import com.cinema.api.model.entity.Poster;
import com.cinema.api.service.PosterService;
import com.cinema.api.utils.BaseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "首页轮播海报接口")
@RestController
@RequestMapping("/api/poster")
public class PosterController {

    @Resource
    private PosterService posterService;

    @GetMapping("")
    @ApiOperation("获取所有海报")
    public List<Poster> posterList(Boolean status) {
        if (BaseUtil.isNotEmpty(status)) {
            // 用户端获取 上架海报
            return posterService.findByStatus(status);
        }
        return posterService.findAll();
    }

    @PostMapping("")
    @ApiOperation("新增/更新首页海报")
    public void saveAndUpdate(@RequestBody Poster poster) {
        posterService.saveAndUpdate(poster);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(("根据ID删除轮播海报"))
    public void deleteById(@PathVariable String id) {
        posterService.deleteById(id);
    }

    @DeleteMapping("")
    @ApiOperation(("删除所有海报"))
    public void deleteAll() {
        posterService.deleteAll();
    }
}
