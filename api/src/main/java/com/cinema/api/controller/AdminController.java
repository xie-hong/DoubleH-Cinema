package com.cinema.api.controller;


import com.cinema.api.model.entity.Admin;
import com.cinema.api.model.vo.LoginVO;
import com.cinema.api.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api(tags = "管理员接口")
@RequestMapping("/api/admin")
public class AdminController {
    @Resource
    private AdminService adminService;

    @PostMapping("/login")
    @ApiOperation("管理员登录")
    public Map<String, Object> login(@RequestBody LoginVO loginVO) throws Exception{
        HashMap<String, Object> map = new HashMap<>();
        // 获取管理员信息并返回
        Admin admin = adminService.getAdmin(loginVO);
        // 进行用户信息校验 以及 生产Token并返回
        map.put("admin", admin);
        map.put("token", adminService.login(loginVO));
        return map;
    }
}
