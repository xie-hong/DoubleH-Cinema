package com.cinema.api.controller.cp;


import com.cinema.api.model.dto.WorkerEvaluateDTO;
import com.cinema.api.model.entity.WorkerEvaluate;
import com.cinema.api.service.WorkerEvaluateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api(tags = "用户端 - 员工评价接口")
@RequestMapping("/api/cp/we")
public class WorkerEvaluateCpController {

    @Resource
    private WorkerEvaluateService workerEvaluateService;

    @GetMapping("")
    @ApiOperation(value = "根据员工Id查看员工客服评价")
    public List<WorkerEvaluateDTO> findById(@RequestParam String wid) {
        return workerEvaluateService.findById(wid);
    }

    @PostMapping("")
    @ApiOperation(value = "添加客服评价")
    public void add(@RequestBody WorkerEvaluate workerEvaluate) {
        workerEvaluateService.add(workerEvaluate);
    }
}
