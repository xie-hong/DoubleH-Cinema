package com.cinema.api.controller;


import com.cinema.api.model.entity.OrderException;
import com.cinema.api.service.OrderExceptionService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "订单异常接口")
@RestController
@RequestMapping("/api/oe")
public class OrderExceptionController {

    @Resource
    private OrderExceptionService orderExceptionService;

    @PostMapping("")
    @ApiOperation("添加异常订单")
    public void createOrUpdate(@RequestBody OrderException orderException) {
        orderExceptionService.createOrUpdate(orderException);
    }

    @GetMapping("")
    @ApiOperation("查询所有异常订单")
    public List<OrderException> findAll() {
        return orderExceptionService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询所有异常订单")
    public PageInfo<OrderException> findAllByPage(@RequestParam int page, @RequestParam int limit) {
        return orderExceptionService.findAllByPage(page, limit);
    }

    @GetMapping("/like")
    @ApiOperation("根据上报人模糊-分页查询所有异常订单")
    public PageInfo<OrderException> filmLikeList(@RequestParam Map<String, Object> params) {
        int page = Integer.parseInt(params.get("page").toString());
        int limit = Integer.parseInt(params.get("limit").toString());
        String reviewer = (String) params.get("reviewer");
        return orderExceptionService.filmLikeList(page, limit, reviewer);
    }

}
