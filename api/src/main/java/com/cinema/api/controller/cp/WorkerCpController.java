package com.cinema.api.controller.cp;


import com.cinema.api.constant.Roles;
import com.cinema.api.model.entity.Role;
import com.cinema.api.model.entity.Worker;
import com.cinema.api.model.vo.LoginVO;
import com.cinema.api.service.RoleService;
import com.cinema.api.service.WorkerService;
import com.cinema.api.utils.JwtUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Api(tags = "用户端 - 员工接口")
@RestController
@RequestMapping("/api/cp/worker")
public class WorkerCpController {

    @Resource
    private WorkerService workerService;

    @Resource
    private RoleService roleService;

    @GetMapping("")
    @ApiOperation("查询全部员工")
    public List<Worker> findAll() {
        return workerService.findAll();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据id查询员工信息")
    public Worker findById(@PathVariable String id) {
        return workerService.findById(id);
    }

   }
