package com.cinema.api.controller;


import com.cinema.api.model.dto.OrderDTO;
import com.cinema.api.model.entity.Order;
import com.cinema.api.service.OrderService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/{fid}")
    @ApiOperation("统计电影票房")
    public String getPrices(@PathVariable String fid) {
        return orderService.getPrices(fid);
    }

    @GetMapping("")
    @ApiOperation(value = "查询所有订单")
    public List<OrderDTO> findAll() throws ParseException {
        return orderService.findAll();
    }

    @GetMapping("/page")
    @ApiOperation(value = "分页查询所有订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value="页码", paramType = "query" ,required = true, defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value="每页条数", paramType = "query",required = true, defaultValue = "10"),
    })
    public PageInfo<OrderDTO> findAllByPage(@RequestParam int page, @RequestParam int limit) throws ParseException {
        return orderService.findAllByPage(page, limit);
    }



    @PutMapping("")
    @ApiOperation(value = "更新订单状态")
    public void update(@RequestBody Order order) {
        orderService.update(order);
    }

}
