package com.cinema.api.controller.cp;


import com.cinema.api.model.entity.Activity;
import com.cinema.api.model.entity.Registration;
import com.cinema.api.service.RegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@RestController
@Api(tags = "用户端 - 活动报名接口")
@RequestMapping("/api/cp/registration")
public class RegistrationCpController {

    @Resource
    private RegistrationService registrationService;

    @PostMapping("")
    @ApiOperation("用户报名活动")
    public void sign(@RequestBody Registration registration) {
        registrationService.sign(registration);
    }


}
