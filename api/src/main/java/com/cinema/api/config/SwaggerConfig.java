package com.cinema.api.config;

import com.cinema.api.annotation.DisableBaseResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    @DisableBaseResponse
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //.groupName("xhh")  //配置多个文档的分组标识
                //.enable(true)    //是否开启（生成环境建议隐藏）
                .select()
                //扫描路径包
                .apis(RequestHandlerSelectors.basePackage("com.cinema.api.controller"))
                //指定路径处理
                .paths(PathSelectors.any())
                .build();
    }

    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //设置文档标题
                .title("贰泓影院系统-Swagger2-API接口文档")
                //文档描述
                .description("贰泓影院接口文档")
                //服务条款URL
                .termsOfServiceUrl("http://localhost:8888/")
                //版本号
                .version("V1.0.0")
                .build();
    }
}
