package com.cinema.api.service;

import com.cinema.api.model.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.model.vo.LoginVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface UserService extends IService<User> {

    User getLogin(LoginVO vo) throws Exception;

    List<User> findAll();

    void updateUser(User user);

    void register(User user);

    PageInfo<User> findAllByPage(int page, int limit);


    User findByUserName(String username);

    User findByNameAndPhone(String username ,String phone);

    void reset(String username, String password);

    User findById(String id);

    void updateBySelf(User user);

    PageInfo<User> findByLike(int page, int limit, String username);
}
