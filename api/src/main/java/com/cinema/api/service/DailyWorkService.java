package com.cinema.api.service;

import com.cinema.api.model.entity.DailyWork;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface DailyWorkService extends IService<DailyWork> {

    List<DailyWork> findAll();

    void add(DailyWork dailyWork);

    void deleteById(String id);

    PageInfo<DailyWork> findAllByPage(int page, int limit);
}
