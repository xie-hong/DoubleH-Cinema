package com.cinema.api.service;

import com.cinema.api.model.dto.ActiveUserDTO;
import com.cinema.api.model.dto.LeavingMessageDTO;
import com.cinema.api.model.entity.LeavingMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface LeavingMessageService extends IService<LeavingMessage> {

    List<LeavingMessageDTO> findAll();

    void addOrReply(LeavingMessage leavingMessage);

    void deleteById(String id);

    List<ActiveUserDTO> findActiveUser();

    PageInfo<LeavingMessageDTO> findAllByPage(int page, int limit);

}
