package com.cinema.api.service;

import com.cinema.api.model.entity.Poster;
import com.baomidou.mybatisplus.extension.service.IService;
import javafx.geometry.Pos;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface PosterService extends IService<Poster> {

    List<Poster> findAll();

    List<Poster> findByStatus(boolean status);

    void saveAndUpdate(Poster poster);

    void deleteById(String id);

    void deleteAll();

}
