package com.cinema.api.service;

import com.cinema.api.model.dto.CartDTO;
import com.cinema.api.model.entity.Cart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface CartService extends IService<Cart> {

    void add(Cart cart);

    List<CartDTO> findAllByUid(String uid);

    void deleteById(String id);
}
