package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.UserMapper;
import com.cinema.api.model.dto.ActiveUserDTO;
import com.cinema.api.model.dto.LeavingMessageDTO;
import com.cinema.api.model.entity.LeavingMessage;
import com.cinema.api.mapper.LeavingMessageMapper;
import com.cinema.api.model.entity.User;
import com.cinema.api.service.LeavingMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.rmi.activation.ActivationDesc;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class LeavingMessageServiceImpl extends ServiceImpl<LeavingMessageMapper, LeavingMessage> implements LeavingMessageService {

    @Resource
    private LeavingMessageMapper leavingMessageMapper;

    @Resource
    private UserMapper userMapper;

    /**
     * 获取所有影院留言
     * 返回id、影院留言、相关用户信息
     */
    @Override
    public List<LeavingMessageDTO> findAll() {
        QueryWrapper<LeavingMessage> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0)
                .orderByDesc("create_time");
        List<LeavingMessage> leavingMessageList = leavingMessageMapper.selectList(wrapper);
        List<LeavingMessageDTO> result = new ArrayList<>();
        for (LeavingMessage list : leavingMessageList) {
            User user = userMapper.selectById(list.getUid());
            result.add(new LeavingMessageDTO(list.getId(), list, user));
        }
        return result;
    }

    /**
     * 分页获取所有影院留言
     */
    @Override
    public PageInfo<LeavingMessageDTO> findAllByPage(int page, int limit) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        QueryWrapper<LeavingMessage> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0)
                .orderByDesc("create_time");
        List<LeavingMessage> leavingMessageList = leavingMessageMapper.selectList(wrapper);
        List<LeavingMessageDTO> result = new ArrayList<>();
        for (LeavingMessage list : leavingMessageList) {
            User user = userMapper.selectById(list.getUid());
            result.add(new LeavingMessageDTO(list.getId(), list, user));
        }
        return new PageInfo<>(result);
    }

    /**
     * 新增或回复（更新）留言
     */
    @Override
    public void addOrReply(LeavingMessage leavingMessage) {
        if (BaseUtil.isEmpty(leavingMessage.getId())) {
            // 新增
            leavingMessage.setId(UUID.randomUUID().toString());
            leavingMessage.setCreateTime(DataTimeUtil.getNowTimeString());
            leavingMessage.setIsDelete(0);
            leavingMessageMapper.insert(leavingMessage);
        } else {
            // 更新
            leavingMessageMapper.updateById(leavingMessage);
        }
    }

    /**
     * 根据ID删除留言
     */
    @Override
    public void deleteById(String id) {
        leavingMessageMapper.deleteById(id);
    }

    /**
     * 获取活跃留言的用户
     */
    @Override
    public List<ActiveUserDTO> findActiveUser() {
        List<ActiveUserDTO> result = new ArrayList<>();
        // 获取所有用户的集合
        List<User> users = userMapper.selectList(null);
        // 对所有的用户进行筛选，统计其留言次数
        for (User user : users) {
            ActiveUserDTO activeUserDTO = new ActiveUserDTO();
            activeUserDTO.setUser(user);
            // 统计留言次数
            QueryWrapper<LeavingMessage> wrapper = new QueryWrapper<>();
            wrapper.in("uid", user.getId());
            int number = leavingMessageMapper.selectList(wrapper).size();
            activeUserDTO.setNumber(number);
            result.add(activeUserDTO);
        }
        // 按留言数量进行降序返回
        result.sort((v1, v2) -> v2.getNumber().compareTo(v1.getNumber()));
        return result;
    }




}
