package com.cinema.api.service;

import com.cinema.api.model.dto.OrderDTO;
import com.cinema.api.model.entity.Cart;
import com.cinema.api.model.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface OrderService extends IService<Order> {

    String getPrices(String fid);

    List<OrderDTO> findAll() throws ParseException;

    PageInfo<OrderDTO> findAllByPage(int page, int limit) throws ParseException;

    void update(Order order);

    void create(Cart cart);

    List<OrderDTO> findByUid(String uid) throws ParseException;

    void deleteById(String id);

    void payById(String id);
}
