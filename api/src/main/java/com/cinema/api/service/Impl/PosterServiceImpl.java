package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.model.entity.DailyWork;
import com.cinema.api.model.entity.Poster;
import com.cinema.api.mapper.PosterMapper;
import com.cinema.api.model.enums.ResultCodeEnum;
import com.cinema.api.model.exception.BizException;
import com.cinema.api.service.PosterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class PosterServiceImpl extends ServiceImpl<PosterMapper, Poster> implements PosterService {

    @Resource
    private PosterMapper posterMapper;

    /**
     *  获取所有轮播海报
     */
    @Override
    public List<Poster> findAll() {
        // 时间格式转化：
        // for (PosterVO poster : posters) {
        //     String createTime = poster.getCreateTime();
        //     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //     String format = simpleDateFormat.format(createTime);
        //     poster.setCreateTime(format);
        // }
        QueryWrapper<Poster> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        List<Poster> posters = posterMapper.selectList(wrapper);
        return posters;
    }

    /**
     *  根据上架状态获取所有封面
     */
    @Override
    public List<Poster> findByStatus(boolean status) {
        QueryWrapper<Poster> wrapper = new QueryWrapper<>();
        wrapper.in("status", status)
                .eq("is_delete", 0);
        return posterMapper.selectList(wrapper);
    }

    /**
     *  新增/更新首页海报
     */
    @Override
    public void saveAndUpdate(Poster poster) {
        if (BaseUtil.isEmpty(poster.getId())) {
            // 新增
            poster.setId(UUID.randomUUID().toString());
            poster.setCreateTime(DataTimeUtil.getNowTimeString());
            poster.setIsDelete(0);
            posterMapper.insert(poster);
        } else {
            // 更新
            posterMapper.updateById(poster);
        }
    }

    /**
     *   根据ID删除海报
     */
    @Override
    public void deleteById(String id) {
        posterMapper.deleteById(id);
    }

    /**
     *   删除所有海报
     */
    @Override
    public void deleteAll() {
        QueryWrapper<Poster> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        List<Poster> posters = posterMapper.selectList(wrapper);
        if (BaseUtil.isEmpty(posters)) {
            throw new BizException(ResultCodeEnum.DATA_IS_EMPTY);
        }
        List<String> ids = new ArrayList<>();
        for (Poster poster : posters) {
            ids.add(poster.getId());
        }
        posterMapper.deleteBatchIds(ids);
    }
}
