package com.cinema.api.service;

import com.cinema.api.model.entity.Upload;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface UploadService extends IService<Upload> {

    String checkAndSaveUpload(MultipartFile file);
}
