package com.cinema.api.service;

import com.cinema.api.model.dto.ArrangementDTO;
import com.cinema.api.model.entity.Arrangement;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface ArrangementService extends IService<Arrangement> {

    void addOrUpdate(Arrangement arrangement);

    void UpdateByPrice(String fid, String price);

    List<Arrangement> findAll();

    Arrangement findById(String id);

    void deleteById(String id);

    ArrangementDTO findByFid(String fid);

    List<Integer> getSeatsHaveSelect(String id);

    PageInfo<Arrangement> findAllByPage(int page, int limit);

    PageInfo<Arrangement> findAllLike(int page, int limit, String name);

}
