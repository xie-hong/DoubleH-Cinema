package com.cinema.api.service;

import com.cinema.api.model.dto.FilmEvaluateDTO;
import com.cinema.api.model.entity.Film;
import com.cinema.api.model.entity.FilmEvaluate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface FilmEvaluateService extends IService<FilmEvaluate> {

    void add(FilmEvaluate filmEvaluate) throws Exception;

    List<FilmEvaluateDTO> findByFid(String fid);


}
