package com.cinema.api.service.Impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.ArrangementMapper;
import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.DailyWork;
import com.cinema.api.model.entity.Film;
import com.cinema.api.mapper.FilmMapper;
import com.cinema.api.model.exception.BizException;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.service.FilmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;

import com.cinema.api.utils.DataTimeUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class FilmServiceImpl extends ServiceImpl<FilmMapper, Film> implements FilmService {

    @Resource
    private FilmMapper filmMapper;

    @Resource
    private ArrangementMapper arrangementMapper;

    /**
     * 用户根据区域和类型查询电影
     */
    @Override
    public List<FilmVO> findByRegionAndType(String region, String type) {
        return filmMapper.findByRegionAndType(region, type);
    }

    /**
     *  返回上架的电影列表
     */
    @Override
    public List<FilmVO> findByStatus(String status, String release) throws ParseException {
        List<FilmVO> films = filmMapper.findByStatus(status, release);
        String format = "";
        for (FilmVO film : films) {
            String releaseTime = film.getReleaseTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse(releaseTime);
            format = simpleDateFormat.format(parse);
            film.setReleaseTime(format);
            Arrangement arrangement = arrangementMapper.findOneByFid(film.getId());
            String boxOffice = "";
            if (BaseUtil.isEmpty(arrangement)) {
                // 部分电影可能没有排片 避免空指针异常
                boxOffice = "0.00";
            } else {
                boxOffice = arrangement.getBoxOffice();
            }
            film.setBoxOffice(boxOffice);
        }
        return films;
    }

    /**
     * 列出所有电影
     */
    // @Cacheable(cacheNames = "findAll")
    @Override
    public List<FilmVO> findAll() {
        return filmMapper.findAll();
    }

    /**
     * 分页
     */
    @Override
    public PageInfo<FilmVO> findAllByPage(int page, int limit) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        List<FilmVO> filmVOS = filmMapper.findAll();
        return new PageInfo<>(filmVOS);
    }

    /**
     *  根据电影名模糊查询
     */
    @Override
    public PageInfo<FilmVO> findByLike(int page, int limit, String name) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        List<FilmVO> like = filmMapper.findByLike(name);
        return new PageInfo<>(like);
    }

    // @CacheEvict(cacheNames = "findAll")
    @Override
    public void saveOrEdit(Film film) {
        if (BaseUtil.isEmpty(film.getId())) {
            // 新增
            QueryWrapper<Film> wrapper = new QueryWrapper<>();
            wrapper.eq("name", film.getName()).eq("is_delete", 0);
            Film name = filmMapper.selectOne(wrapper);
            if (BaseUtil.isNotEmpty(name)) {
                throw new BizException(1001,"电影名称已存在");
            }
            film.setHot(0);
            film.setIsDelete(0);
            film.setCreateTime(DataTimeUtil.getNowTimeString());
            filmMapper.insert(film);
        } else {
            // 更新编辑
            String name = filmMapper.selectByEditName(film.getName(), film.getId());
            if (BaseUtil.isNotEmpty(name)) {
                throw new BizException(1001,"电影名称已存在");
            }
            film.setUpdateTime(DataTimeUtil.getNowTimeString());
            filmMapper.updateById(film);
        }
    }

    /**
     * 根据Id删除电影
     */
    @Override
    public void deleteById(String id) {
        filmMapper.deleteById(id);
    }

    /**
     * 根据id查询电影信息
     */
    // @Cacheable
    @Override
    public Film findById(String id) throws ParseException {
        QueryWrapper<Film> wrapper = new QueryWrapper<>();
        wrapper.eq("id", id)
                .eq("is_delete", 0);
        Film film = filmMapper.selectOne(wrapper);
        String format = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = simpleDateFormat.parse(film.getReleaseTime());
        format = simpleDateFormat.format(parse);
        film.setReleaseTime(format);
        return film;
    }

    /**
     * 搜索名称模糊查询电影
     */
    // @Cacheable
    @Override
    public List<Film> findByName(String name) {
        QueryWrapper<Film> wrapper = new QueryWrapper<>();
        wrapper.like("name", name)
                .eq("is_delete", 0);
        return filmMapper.selectList(wrapper);
    }

    /**
     *  获取热榜前10的电影
     */
    @Override
    public List<Film> hotList(Integer limit) throws ParseException {
        QueryWrapper<Film> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("hot")
                .in("status", 1)
                .eq("is_delete", 0)
                // 手动拼接sql到后面
                .last("limit " + limit);
        List<Film> films = filmMapper.selectList(wrapper);
        String format = "";
        for (Film film : films) {
            String releaseTime = film.getReleaseTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse(releaseTime);
            format = simpleDateFormat.format(parse);
            film.setReleaseTime(format);
        }
        return films;
    }

    /**
     * 获取票房前10的电影
     */
    @Override
    public List<FilmVO> boxOfficeList(Integer limit) throws ParseException {
        List<FilmVO> result = filmMapper.boxOfficeList(limit);
        String format = "";
        for (FilmVO vo : result) {
            String releaseTime = vo.getReleaseTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = simpleDateFormat.parse(releaseTime);
            format = simpleDateFormat.format(parse);
            vo.setReleaseTime(format);
        }
        return result;
    }


}
