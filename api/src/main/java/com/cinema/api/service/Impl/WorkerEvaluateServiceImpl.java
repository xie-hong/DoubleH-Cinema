package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.UserMapper;
import com.cinema.api.model.dto.WorkerEvaluateDTO;
import com.cinema.api.model.entity.WorkerEvaluate;
import com.cinema.api.mapper.WorkerEvaluateMapper;
import com.cinema.api.service.WorkerEvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.DataTimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class WorkerEvaluateServiceImpl extends ServiceImpl<WorkerEvaluateMapper, WorkerEvaluate> implements WorkerEvaluateService {

    @Resource
    private WorkerEvaluateMapper workerEvaluateMapper;

    @Resource
    private UserMapper userMapper;

    /**
     * 根据员工Id查看客服评价
     */
    @Override
    public List<WorkerEvaluateDTO> findById(String wid) {
        QueryWrapper<WorkerEvaluate> wrapper = new QueryWrapper<>();
        wrapper.in("wid", wid)
                .eq("is_delete", 0);
        List<WorkerEvaluate> workerEvaluates = workerEvaluateMapper.selectList(wrapper);
        List<WorkerEvaluateDTO> result = new ArrayList<>();
        for (WorkerEvaluate workerEvaluate : workerEvaluates) {
            WorkerEvaluateDTO workerEvaluateDTO = new WorkerEvaluateDTO();
            // 评价信息
            workerEvaluateDTO.setWorkerEvaluate(workerEvaluate);
            // 根据UID获取用户信息
            workerEvaluateDTO.setUser(userMapper.selectById(workerEvaluate.getUid()));
            result.add(workerEvaluateDTO);
        }
        return result;
    }

    /**
     * 添加客服评价
     */
    @Override
    public void add(WorkerEvaluate workerEvaluate) {
        workerEvaluate.setCreateTime(DataTimeUtil.getNowTimeString());
        workerEvaluate.setId(UUID.randomUUID().toString());
        workerEvaluate.setIsDelete(0);
        workerEvaluateMapper.insert(workerEvaluate);
    }
}
