package com.cinema.api.service;

import com.cinema.api.model.entity.Activity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface ActivityService extends IService<Activity> {

    List<Activity> findAll();

    void add(Activity activity);

    Activity findById(String id);

    void deleteById(String id);

    List<Activity> findAllForUser();


}
