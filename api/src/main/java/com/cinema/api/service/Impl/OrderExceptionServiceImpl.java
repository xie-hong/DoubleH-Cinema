package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.OrderMapper;
import com.cinema.api.model.entity.Order;
import com.cinema.api.model.entity.OrderException;
import com.cinema.api.mapper.OrderExceptionMapper;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.service.OrderExceptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.service.OrderService;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class OrderExceptionServiceImpl extends ServiceImpl<OrderExceptionMapper, OrderException> implements OrderExceptionService {

    @Resource
    private OrderExceptionMapper orderExceptionMapper;

    @Resource
    private OrderService orderService;

    @Resource
    private OrderMapper orderMapper;

    /**
     * 创建异常订单
     */
    @Override
    public void createOrUpdate(OrderException orderException) {
        if (BaseUtil.isEmpty(orderException.getId())){
            // 创建
            // 订单状态为未处理
            orderException.setStatus(false);
            orderException.setId(UUID.randomUUID().toString());
            orderException.setCreateTime(DataTimeUtil.getNowTimeString());
            orderException.setIsDelete(0);
            orderExceptionMapper.insert(orderException);
            // 订单状态变为订单异常：4
            Order order = orderMapper.selectById(orderException.getOid());
            order.setStatus(4);
            orderService.update(order);
        } else {
            orderException.setEndTime(DataTimeUtil.getNowTimeString());
            orderExceptionMapper.updateById(orderException);
        }

    }

    /**
     * 查询所有异常订单
     */
    @Override
    public List<OrderException> findAll() {
        QueryWrapper<OrderException> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        return orderExceptionMapper.selectList(wrapper);
    }

    /**
     * 分页查询所有异常订单
     */
    @Override
    public PageInfo<OrderException> findAllByPage(int page, int limit) {
        PageHelper.startPage(page, limit);
        QueryWrapper<OrderException> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        List<OrderException> orderExceptions = orderExceptionMapper.selectList(wrapper);
        return new PageInfo<>(orderExceptions);
    }

    @Override
    public PageInfo<OrderException> filmLikeList(int page, int limit, String reviewer) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        List<OrderException> like = orderExceptionMapper.findByLike(reviewer);
        return new PageInfo<>(like);    }


}
