package com.cinema.api.service;

import com.cinema.api.model.entity.Activity;
import com.cinema.api.model.entity.Registration;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface RegistrationService extends IService<Registration> {

    void sign(Registration registration);
}
