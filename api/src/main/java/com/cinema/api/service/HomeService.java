package com.cinema.api.service;

import com.cinema.api.model.vo.HomeVO;

import java.util.List;

public interface HomeService {
    HomeVO findHome();
}
