package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.*;
import com.cinema.api.model.dto.FilmEvaluateDTO;
import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.model.entity.Film;
import com.cinema.api.model.entity.FilmEvaluate;
import com.cinema.api.model.entity.Order;
import com.cinema.api.model.exception.BizException;
import com.cinema.api.service.FilmEvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.apache.ibatis.ognl.OgnlOps.in;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class FilmEvaluateServiceImpl extends ServiceImpl<FilmEvaluateMapper, FilmEvaluate> implements FilmEvaluateService {

    @Resource
    private FilmEvaluateMapper filmEvaluateMapper;

    @Resource
    private FilmMapper filmMapper;

    @Resource
    private ArrangementMapper arrangementMapper;

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private UserMapper userMapper;

    /**
     * 新增电影评论
     */
    @Override
    public void add(FilmEvaluate filmEvaluate) throws Exception{
        QueryWrapper<Arrangement> wrapper1 = new QueryWrapper<>();
        wrapper1.in("fid", filmEvaluate.getFid())
                .eq("is_delete", 0);
        List<Arrangement> arrangements = arrangementMapper.selectList(wrapper1);
        for (Arrangement arrangement : arrangements) {
            // 判断用户是否观看、购买过该影片
            QueryWrapper<Order> wrapper = new QueryWrapper<>();
            wrapper.in("aid", arrangement.getId())
                    .in("uid", filmEvaluate.getUid())
                    .eq("is_delete", 0);
            Order order = orderMapper.selectOne(wrapper);
            if (BaseUtil.isEmpty(order)) {
                // 用户还未购买该电影
                throw new BizException(2001, "您还未购票，不能对此电影进行评分！");
            }
        }
        QueryWrapper<FilmEvaluate> wrapper2 = new QueryWrapper<>();
        wrapper2.in("fid", filmEvaluate.getFid())
                .in("uid", filmEvaluate.getUid())
                .eq("is_delete", 0);
        FilmEvaluate evaluate = filmEvaluateMapper.selectOne(wrapper2);
        if (BaseUtil.isNotEmpty(evaluate)) {
            // 单个用户对单个电影不能重复添加评价
            throw new BizException(2001,"感谢您的参与，您已对此电影进行评分!" + "   请不要重复操作！");
        }
        // 新增评价
        filmEvaluate.setId(UUID.randomUUID().toString());
        filmEvaluate.setCreateTime(DataTimeUtil.getNowTimeString());
        filmEvaluate.setIsDelete(0);
        filmEvaluateMapper.insert(filmEvaluate);
        // 电影评论增加，电影热度+1
        Film film = filmMapper.selectById(filmEvaluate.getFid());
        film.setHot(film.getHot() + 1);
        filmMapper.updateById(film);
    }

    /**
     * 获取(根据Fid)电影评论
     * 返回用户信息以及该电影评论
     */
    @Override
    public List<FilmEvaluateDTO> findByFid(String fid) {
        List<FilmEvaluateDTO> result = new ArrayList<>();
        QueryWrapper<FilmEvaluate> wrapper = new QueryWrapper<>();
        wrapper.in("fid", fid)
                .eq("is_delete", 0);
        List<FilmEvaluate> filmEvaluates = filmEvaluateMapper.selectList(wrapper);
        for (FilmEvaluate filmEvaluate : filmEvaluates) {
            FilmEvaluateDTO dto = new FilmEvaluateDTO();
            dto.setUser(userMapper.selectById(filmEvaluate.getUid()));
            dto.setFilmEvaluate(filmEvaluate);
            dto.setId(filmEvaluate.getId());
            result.add(dto);
        }
        return result;
    }
}
