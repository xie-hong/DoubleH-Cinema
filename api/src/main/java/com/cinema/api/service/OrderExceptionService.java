package com.cinema.api.service;

import com.cinema.api.model.entity.OrderException;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface OrderExceptionService extends IService<OrderException> {

    void createOrUpdate(OrderException orderException);

    List<OrderException> findAll();

    PageInfo<OrderException> findAllByPage(int page, int limit);

    PageInfo<OrderException> filmLikeList(int page, int limit, String reviewer);
}
