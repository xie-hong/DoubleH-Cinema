package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.model.entity.Activity;
import com.cinema.api.mapper.ActivityMapper;
import com.cinema.api.service.ActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {

    @Resource
    private ActivityMapper activityMapper;

    /**
     * 获取全部活动信息
     */
    @Override
    public List<Activity> findAll() {
        return activityMapper.selectList(null);
    }

    /**
     * 新增活动
     */
    @Override
    public void add(Activity activity) {
        if (BaseUtil.isEmpty(activity.getId())) {
            activity.setId(UUID.randomUUID().toString());
            activity.setCreateTime(DataTimeUtil.getNowTimeString());
            activity.setIsDelete(0);
            activityMapper.insert(activity);
        } else {
            activityMapper.updateById(activity);
        }

    }

    /**
     * 根据id查找活动
     */
    @Override
    public Activity findById(String id) {
        return activityMapper.selectById(id);
    }

    /**
     * 根据ID删除活动
     */
    @Override
    public void deleteById(String id) {
        activityMapper.deleteById(id);
    }

    /**
     * 用户端获取全部活动（大于当前时间）
     */
    @Override
    public List<Activity> findAllForUser() {
        QueryWrapper<Activity> wrapper = new QueryWrapper<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String now = simpleDateFormat.format(new Date());
        wrapper.eq("is_delete", 0)
                .orderByDesc("start_time");
        return activityMapper.selectList(wrapper);
    }
}
