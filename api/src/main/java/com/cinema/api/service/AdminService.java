package com.cinema.api.service;

import com.cinema.api.model.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.api.model.vo.LoginVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface AdminService extends IService<Admin> {

    String login(LoginVO loginVO) throws Exception;

    Admin getAdmin(LoginVO loginVO) throws Exception;
}
