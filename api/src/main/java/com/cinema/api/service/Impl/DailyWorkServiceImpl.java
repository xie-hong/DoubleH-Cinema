package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.model.entity.Admin;
import com.cinema.api.model.entity.DailyWork;
import com.cinema.api.mapper.DailyWorkMapper;
import com.cinema.api.service.DailyWorkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.DataTimeUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class DailyWorkServiceImpl extends ServiceImpl<DailyWorkMapper, DailyWork> implements DailyWorkService {

    @Resource
    private DailyWorkMapper dailyWorkMapper;

    /**
     * 查询所有每日工作
     */
    @Override
    public List<DailyWork> findAll() {
        QueryWrapper<DailyWork> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        return dailyWorkMapper.selectList(wrapper);
    }

    /**
     * 分页查询所有每日工作
     */
    @Override
    public PageInfo<DailyWork> findAllByPage(int page, int limit) {
        PageHelper.startPage(page, limit);
        QueryWrapper<DailyWork> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        List<DailyWork> dailyWorks = dailyWorkMapper.selectList(wrapper);
        return new PageInfo<>(dailyWorks);
    }

    /**
     * 添加每日目标
     */
    @Override
    public void add(DailyWork dailyWork) {
        dailyWork.setId(UUID.randomUUID().toString());
        dailyWork.setCreateTime(DataTimeUtil.getNowTimeString());
        dailyWork.setIsDelete(0);
        dailyWorkMapper.insert(dailyWork);
    }

    /**
     * 根据id删除每日工作目标
     */
    @Override
    public void deleteById(String id) {
        dailyWorkMapper.deleteById(id);
    }


}
