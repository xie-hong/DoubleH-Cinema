package com.cinema.api.service;

import com.cinema.api.model.entity.Worker;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.api.model.vo.LoginVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface WorkerService extends IService<Worker> {

    Worker login(LoginVO loginVO) throws Exception;

    List<Worker> findAll();

    void createOrUpdate(Worker worker) throws Exception;

    Worker findById(String  id);

    void deleteById(String id);

    PageInfo<Worker> findAllByPage(int page, int limit);

    PageInfo<Worker> filmLikeList(int page, int limit, String username);

}
