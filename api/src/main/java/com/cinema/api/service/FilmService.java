package com.cinema.api.service;

import com.cinema.api.model.entity.Film;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cinema.api.model.vo.FilmVO;
import com.github.pagehelper.PageInfo;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface FilmService extends IService<Film> {

    List<FilmVO> findByRegionAndType(String region, String type);

    List<FilmVO> findByStatus(String status, String release) throws ParseException;

    List<FilmVO> findAll();

    PageInfo<FilmVO> findAllByPage(int page, int limit);

    void saveOrEdit(Film film);

    void deleteById(String id);

    Film findById(String id) throws ParseException;

    List<Film> findByName(String name);

    List<Film> hotList(Integer limit) throws ParseException;

    List<FilmVO> boxOfficeList(Integer limit) throws ParseException;


    PageInfo<FilmVO> findByLike(int page, int limit, String name);
}
