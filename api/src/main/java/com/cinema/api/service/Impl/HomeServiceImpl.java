package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.mapper.ArrangementMapper;
import com.cinema.api.mapper.FilmMapper;
import com.cinema.api.model.entity.*;
import com.cinema.api.model.vo.HomeVO;
import com.cinema.api.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Resource
    private UserService userService;
    @Resource
    private FilmService filmService;
    @Resource
    private ArrangementService arrangementService;
    @Resource
    private ArrangementMapper arrangementMapper;
    @Resource
    private LeavingMessageService leavingMessageService;
    @Resource
    private WorkerEvaluateService workerEvaluateService;
    @Resource
    private OrderService orderService;

    @Override
    public HomeVO findHome() {
        List<HomeVO> result = new ArrayList<>();
        HomeVO homeVO = new HomeVO();
        QueryWrapper<User> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("is_delete", 0);
        homeVO.setUser(userService.count(wrapper1));
        QueryWrapper<Film> wrapper2 = new QueryWrapper<>();
        wrapper2.eq("is_delete", 0);
        homeVO.setFilm(filmService.count(wrapper2));
        QueryWrapper<Arrangement> wrapper3 = new QueryWrapper<>();
        wrapper3.eq("is_delete", 0);
        homeVO.setArrangement(arrangementService.count(wrapper3));
        QueryWrapper<LeavingMessage> wrapper4 = new QueryWrapper<>();
        wrapper4.eq("is_delete", 0);
        homeVO.setLeaving(leavingMessageService.count(wrapper4));
        QueryWrapper<WorkerEvaluate> wrapper5 = new QueryWrapper<>();
        wrapper5.eq("is_delete", 0);
        homeVO.setEvaluate(workerEvaluateService.count(wrapper5));
        QueryWrapper<Order> wrapper6 = new QueryWrapper<>();
        wrapper6.eq("is_delete", 0);
        homeVO.setOrder(orderService.count(wrapper6));
        Integer office = arrangementMapper.getOffice();
        homeVO.setBoxOffice(office);

        return homeVO;
    }
}
