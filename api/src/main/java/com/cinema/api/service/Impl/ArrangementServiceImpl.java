package com.cinema.api.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cinema.api.mapper.FilmMapper;
import com.cinema.api.mapper.OrderMapper;
import com.cinema.api.model.dto.ArrangementDTO;
import com.cinema.api.model.entity.Arrangement;
import com.cinema.api.mapper.ArrangementMapper;
import com.cinema.api.model.entity.DailyWork;
import com.cinema.api.model.entity.Film;
import com.cinema.api.model.entity.Order;
import com.cinema.api.model.vo.FilmVO;
import com.cinema.api.service.ArrangementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.DataTimeUtil;
import com.cinema.api.utils.SecurityUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
@Service
public class ArrangementServiceImpl extends ServiceImpl<ArrangementMapper, Arrangement> implements ArrangementService {

    @Resource
    private ArrangementMapper arrangementMapper;

    @Resource
    private FilmMapper filmMapper;

    @Resource
    private OrderMapper orderMapper;

    /**
     * 查询所有电影排片
     */
    @Override
    public List<Arrangement> findAll() {
        QueryWrapper<Arrangement> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        return arrangementMapper.selectList(wrapper);
    }

    /**
     * 分页查询所有电影排片
     */
    @Override
    public PageInfo<Arrangement> findAllByPage(int page, int limit) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        QueryWrapper<Arrangement> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete", 0);
        List<Arrangement> arrangements = arrangementMapper.selectList(wrapper);
        return new PageInfo<>(arrangements);
    }

    @Override
    public PageInfo<Arrangement> findAllLike(int page, int limit, String name) {
        // 使用Mybatis分页插件
        PageHelper.startPage(page, limit);
        List<Arrangement> like = arrangementMapper.findByLike(name);
        return new PageInfo<>(like);    }

    /**
     * 根据id查找排片
     */
    @Override
    public Arrangement findById(String id) {
        return arrangementMapper.selectById(id);
    }

    /**
     *  根据id删除排片
     */
    @Override
    public void deleteById(String id) {
        arrangementMapper.deleteById(id);
    }

    /**
     *  根据fid查询对应电影的所有排片信息
     */
    @Override
    public ArrangementDTO findByFid(String fid) {
        QueryWrapper<Arrangement> wrapper = new QueryWrapper<>();
        wrapper.in("fid", fid)
                .eq("is_delete", 0);
        List<Arrangement> arrangements = arrangementMapper.selectList(wrapper);
        Film film = filmMapper.selectById(fid);
        for (int i = 0; i < arrangements.size(); i++) {
                String time = arrangements.get(i).getDate()+' '+ arrangements.get(i).getStartTime();
                // 时间字符串比较，判断排片开始时间与当前时间大小
                if (time.compareTo(DataTimeUtil.getNowTimeString()) < 1) {
                    // 排片开始时间小于当前时间(移除)
                    arrangements.remove(i);
                    i--;
                }
        }
        return new ArrangementDTO(arrangements, film);
    }

    /**
     * 获取该场次已被选择订购的座位集合
     */
    @Override
    public List<Integer> getSeatsHaveSelect(String id) {
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.in("aid", id)
                .eq("is_delete", 0);
        List<Order> orders = orderMapper.selectList(wrapper);
        List<Integer> selectSeats = new ArrayList<>();
        // 根据该场次订单表数据 分割出已被选择购买的座位号
        for (Order order : orders) {
            String[] split = order.getSeats().split("号");
            for (String s : split) {
                selectSeats.add(Integer.parseInt(s));
            }
        }
        return selectSeats;
    }


    /**
     *  新增或更新电影排片
     */
    @Override
    public void addOrUpdate(Arrangement arrangement) {
        String username = SecurityUtils.getUsername();
        if (BaseUtil.isEmpty(arrangement.getId())) {
            // 新增
            arrangement.setBoxOffice("0");
            arrangement.setCreateTime(new Date());
            arrangement.setCreateName(username);
            arrangement.setIsDelete(0);
            arrangementMapper.insert(arrangement);
        } else {
            // 更新
            arrangementMapper.updateById(arrangement);
        }
    }

    /**
     * 更新排片票房
     */
    @Override
    public void UpdateByPrice(String fid, String price) {
        arrangementMapper.UpdateByPrice(fid, price);
    }


}
