package com.cinema.api.service;

import com.cinema.api.model.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface RoleService extends IService<Role> {

    List<Role> findRoleListById(String wid);

    void deleteById(String id) throws Exception;

    void add(Role role) throws Exception;

    void deleteAllByWID(String wid);

}
