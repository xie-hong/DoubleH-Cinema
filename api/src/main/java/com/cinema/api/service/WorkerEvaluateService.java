package com.cinema.api.service;

import com.cinema.api.model.dto.WorkerEvaluateDTO;
import com.cinema.api.model.entity.WorkerEvaluate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhh
 * @since 2021-12-31
 */
public interface WorkerEvaluateService extends IService<WorkerEvaluate> {

    List<WorkerEvaluateDTO> findById(String wid);

    void add(WorkerEvaluate workerEvaluate);
}
