package com.cinema.api.auth;

import com.cinema.api.model.exception.BizException;
import com.cinema.api.model.result.Result;
import com.cinema.api.utils.BaseUtil;
import com.cinema.api.utils.JwtUtil;
import com.cinema.api.utils.ResponseUtil;
import io.jsonwebtoken.Claims;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Authorization 授权 发放token
 * BasicHttpAuthenticationFilter 继承自抽象类 OncePerRequestFilter
 */
@Slf4j
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    /**
     * @param request
     * @param response
     * @param chain 过滤链
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        // 从Request Header 取出Token
        String token = request.getHeader(JwtUtil.TOKEN_HEADER);
        System.out.println(request.getRequestURI());
        // Token为空的话放行(登录时)
        if (token == null || "null".equals(token) || "undefined".equals(token)) {
            chain.doFilter(request, response);
            return;
        }
        // Claims claims = JwtUtil.getTokenClaims(token);
        // if (claims == null) {
        //     throw new BizException(402, "token异常!!!");
        // }
        // 判断Jwt token是否过期
        if (JwtUtil.isExpiration(token)) {
            ResponseUtil.writeJson(response, Result.error(403,"token令牌已过期，请重新登录"));
            // throw new BizException(403, "Token异常");
            return;
        }
        // 解析Jwt获取用户信息
        String username = JwtUtil.getUsername(token);
        // 获取权限
        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (String role : JwtUtil.getTokenRoles(token)) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        // 向Spring Security的Context中加入认证信息
        SecurityContextHolder.getContext().setAuthentication(
                // 用户名密码权限认证
                new UsernamePasswordAuthenticationToken(username, null, authorities)
        );
        // 进入下个过滤链
        super.doFilterInternal(request, response, chain);
    }
}
