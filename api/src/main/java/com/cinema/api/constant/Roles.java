package com.cinema.api.constant;

/**
 * 权限常量
 */
public final class Roles {
    // 管理员
    public final static String ROLE_ADMIN = "ROLE_ADMIN";
    // 普通用户
    public final static String ROLE_USER = "ROLE_USER";
    // 普通员工
    public final static String ROLE_WORKER = "ROLE_WORKER";
    // 查询用户
    public final static String ROLE_FIND_USER = "ROLE_FIND_USER";
    // 新增电影
    public final static String ROLE_ADD_FILE = "ROLE_ADD_FILE";
    // 新增排片
    public final static String ROLE_ADD_ARRANGEMENT = "ROLE_ADD_ARRANGEMENT";
    // 删除电影
    public final static String ROLE_DELETE_FILE = "ROLE_DELETE_FILE";
    // 删除排片
    public final static String ROLE_DELETE_ARRANGEMENT = "ROLE_DELETE_ARRANGEMENT";
    // 功能权限集合
    public final static String[] ROLES = {
            ROLE_FIND_USER,
            ROLE_ADD_FILE,
            ROLE_ADD_ARRANGEMENT,
            ROLE_DELETE_FILE,
            ROLE_DELETE_ARRANGEMENT
    };


}
