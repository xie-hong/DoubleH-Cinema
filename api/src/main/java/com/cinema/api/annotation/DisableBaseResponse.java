package com.cinema.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 作用: 不需要统一拦截返回JSON的方法
 * 例如: 获取二进制图片 Response需要写入文件流 不需要返回json
 */
@Target(ElementType.METHOD)// 修饰范围
@Retention(RetentionPolicy.RUNTIME)// 保留时间
public @interface DisableBaseResponse {

}
