package com.cinema.api.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.*;

public final class JwtUtil {
    // Token在Http Header中的名字
    public final static String TOKEN_HEADER = "Authorization";

    // 开发测试
    public final static long Test_TIME = 60 * 1000;

    // 一个星期过期
    public final static long REMEMBER_EXPIRATION_TIME = 1000 * 60 * 60 * 24 * 7;

    // 一天过期
    public final static long EXPIRATION_TIME = 1000 * 60 * 60 * 24;

    // 应用密钥
    private static final String APP_SECRET = "movie.api";

    // 角色权限声明
    private static final String ROLE_CLAIMS = "roles";

    /**
     * 生成Token
     */
    public static String createToken(String username, List<String> roles, long expiration) {
        Map<String, Object> map = new HashMap<>();
        map.put(ROLE_CLAIMS, roles);
        return Jwts.builder()
                .signWith(SignatureAlgorithm.HS256, APP_SECRET) // 签名（Signature）
                .setClaims(map) // 声明（Payload）
                .setIssuedAt(new Date()) // 创建时间
                .setSubject(username) //面向的用户
                .setExpiration(new Date(System.currentTimeMillis() + expiration)) // 过期时间
                .compact();
    }

    /**
     * 获取token body
     */
    public static Claims getTokenClaims(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(APP_SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch ( ExpiredJwtException e ) {
            claims = e.getClaims();
        }
        return claims;
    }

    /**
     * 从Token中获取username
     */
    public static String getUsername(String token) {
        String username;
        try {
            Claims claims = getTokenClaims(token);
            username = claims.getSubject();
        } catch ( ExpiredJwtException e ) {
            username = null;
        }
        return username;
    }

    /**
     * 从Token中获取用户角色
     */
    public static List<String> getTokenRoles(String token) {
        List<String> roles = new ArrayList<>();
        Object object = getTokenClaims(token).get(ROLE_CLAIMS);
        if (object instanceof ArrayList<?>) {
            for (Object o : (List<?>) object) {
                roles.add((String) o);
            }
        }
        return roles;
    }

    /**
     * 校验Token是否过期
     */
    public static boolean isExpiration(String token) {
        return getTokenClaims(token).getExpiration().before(new Date());
    }

}
