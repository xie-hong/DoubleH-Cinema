package com.cinema.api.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BaseUtil {
    private static final transient Logger logger = LoggerFactory.getLogger(BaseUtil.class);

    public BaseUtil() {
    }

    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        } else {
            if (o instanceof String) {
                if ("".equals(o.toString().trim())) {
                    return true;
                }
            } else if (o instanceof List) {
                if (((List)o).size() == 0) {
                    return true;
                }
            } else if (o instanceof Map) {
                if (((Map)o).size() == 0) {
                    return true;
                }
            } else if (o instanceof Set) {
                if (((Set)o).size() == 0) {
                    return true;
                }
            } else if (o instanceof Object[]) {
                if (((Object[])((Object[])o)).length == 0) {
                    return true;
                }
            } else if (o instanceof int[]) {
                if (((int[])((int[])o)).length == 0) {
                    return true;
                }
            } else if (o instanceof long[] && ((long[])((long[])o)).length == 0) {
                return true;
            }

            return false;
        }
    }

    public static boolean isEmpty(Object... o) {
        boolean empty = false;
        Object[] var2 = o;
        int var3 = o.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Object o1 = var2[var4];
            if (isEmpty(o1)) {
                empty = true;
                break;
            }
        }

        return empty;
    }

    public static Map putMapLike(Map map, String key, Object value) {
        if (isNotEmpty(value)) {
            map.put(key, "%" + value + "%");
        }

        return map;
    }

    public static Map putMapLeftLike(Map map, String key, Object value) {
        if (isNotEmpty(value)) {
            map.put(key, "%" + value);
        }

        return map;
    }

    public static Map putMapRightLike(Map map, String key, Object value) {
        if (isNotEmpty(value)) {
            map.put(key, value + "%");
        }

        return map;
    }

    public static Map putMap(Map map, String key, Object value) {
        if (isNotEmpty(value)) {
            map.put(key, value);
        }

        return map;
    }

    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }

    public static boolean isOneEmpty(Object... os) {
        int var2 = os.length;
        byte var3 = 0;
        if (var3 < var2) {
            Object o = os[var3];
            return isEmpty(o);
        } else {
            return false;
        }
    }

    public static boolean isAllEmpty(Object... os) {
        Object[] var1 = os;
        int var2 = os.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            Object o = var1[var3];
            if (!isEmpty(o)) {
                return false;
            }
        }

        return true;
    }

    public static String trimSpace(String source) {
        return source == null ? source : source.replaceAll("^[\\s　]*|[\\s　]*$", "");
    }

    public static String getUUID32() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public static StringBuilder appendDepartmentSeparator(StringBuilder input, Long nodeId) {
        return ((StringBuilder)Optional.ofNullable(input).orElse(new StringBuilder())).append(nodeId).append("-");
    }

    public static String addBrackets(String nodeId) {
        if (isNotEmpty(nodeId)) {
            nodeId = nodeId + "-";
        }

        return nodeId;
    }


    public static <T> void setKey(T vo, String key, Object value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (isNotEmpty(value)) {
            Field keyField = null;

            try {
                keyField = vo.getClass().getDeclaredField(key);
            } catch (NoSuchFieldException var6) {
                logger.debug(vo.getClass().getName() + "对象不存在" + key + "字段！");
            }

            if (isNotEmpty(keyField)) {
                String setMethodName = setMethodName(key);
                Method setMethod = vo.getClass().getMethod(setMethodName, value.getClass());
                setMethod.invoke(vo, value);
            }
        }

    }

    public static String getMethodName(String key) {
        return "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
    }

    public static String setMethodName(String key) {
        return "set" + key.substring(0, 1).toUpperCase() + key.substring(1);
    }



    public static Integer getDateVersion(Integer version) {
        Integer dateVersion = 0;
        if (isNotEmpty(version)) {
            dateVersion = version;
        }

        dateVersion = dateVersion + 1;
        return dateVersion;
    }

    public static <T> void setCreatePO(T vo, String userId, String orgId) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        setKey(vo, "createTime", new Date());
        setKey(vo, "createUser", userId);
        setKey(vo, "orgId", orgId);
        setKey(vo, "isDelete", "0");
        setKey(vo, "dateVersion", 0);
    }

    public static String appendMoreSql(Object object) {
        if (object != null && (object instanceof List || object instanceof String[])) {
            StringBuffer sql = new StringBuffer(" (");
            if (object instanceof List) {
                if (((List)object).size() != 0) {
                    Iterator var2 = ((List)object).iterator();

                    while(var2.hasNext()) {
                        String str = (String)var2.next();
                        sql.append("'").append(str).append("',");
                    }
                }
            } else if (object instanceof String[] && ((String[])((String[])object)).length != 0) {
                String[] var6 = (String[])((String[])object);
                int var7 = var6.length;

                for(int var4 = 0; var4 < var7; ++var4) {
                    String str = var6[var4];
                    sql.append("'").append(str).append("',");
                }
            }

            return sql.substring(0, sql.length() - 1) + ") ";
        } else {
            return "";
        }
    }

    public static String replaceAsterisk(String mobile) {
        if (isNotEmpty(mobile)) {
            String compileStr = "[0-9]*";
            Pattern pattern = Pattern.compile(compileStr);
            Matcher isNum = pattern.matcher(mobile);
            if (isNum.matches() && mobile.length() == 11) {
                String str1 = mobile.substring(0, 3);
                String str2 = mobile.substring(mobile.length() - 4);
                mobile = str1 + "****" + str2;
            }
        }

        return mobile;
    }

    public static String getStringMap(Map map, String key) {
        Object val = map.get(key);
        if (isNotEmpty(val)) {
            return String.valueOf(val);
        } else {
            return val == "" ? "" : null;
        }
    }

    public static List<String> getParentDeptByFullName(String deptFullName) {
        List<String> result = new ArrayList();
        if (deptFullName.contains("-")) {
            String[] split = deptFullName.split("-");
            String dept = "";
            String[] var4 = split;
            int var5 = split.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String s = var4[var6];
                dept = dept + s;
                dept = dept + "-";
                result.add(dept);
            }
        } else {
            result.add(deptFullName);
        }

        return result;
    }

    public static <T> List<T> listCopy(List<T> src) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(src);
        ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut.toByteArray());
        ObjectInputStream in = new ObjectInputStream(byteIn);
        List<T> dest = (List)in.readObject();
        return dest;
    }

    public static String getDatePoor(String startTime, String endTime) {
        String str = "";
        if (null != startTime && null != endTime) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date nowDate = sdf.parse(startTime);
                Date endDate = sdf.parse(endTime);
                long nd = 86400000L;
                long nh = 3600000L;
                long nm = 60000L;
                long diff = endDate.getTime() - nowDate.getTime();
                long day = diff / nd;
                long hour = diff % nd / nh;
                long min = diff % nd % nh / nm;
                if (day > 0L) {
                    str = str + day + "天";
                }

                if (hour > 0L) {
                    str = str + hour + "小时";
                }

                if (min > 0L) {
                    str = str + min + "分钟";
                }
            } catch (ParseException var20) {
                var20.printStackTrace();
            }
        }

        return str;
    }

    public static String dateToWeek(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String[] weekDays = new String[]{"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        Date datet = null;

        try {
            datet = f.parse(datetime);
            cal.setTime(datet);
        } catch (ParseException var6) {
            var6.printStackTrace();
        }

        int w = cal.get(7) - 1;
        if (w < 0) {
            w = 0;
        }

        return weekDays[w];
    }

    public static String subStringDate(String datetime, int n) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateStr = "";

        try {
            if (datetime.length() >= 16) {
                if (n == 0) {
                    dateStr = sdf.format(sdf.parse(datetime)).substring(0, 10);
                }

                if (n == 1) {
                    dateStr = sdf.format(sdf.parse(datetime)).substring(11, 16);
                }
            }
        } catch (ParseException var5) {
            var5.printStackTrace();
        }

        return dateStr;
    }

    public static void setMsgMapValue(Map<String, String> parameters, String first, String keyword1, String keyword2, String keyword3) {
        parameters.put("first", first);
        parameters.put("keyword1", keyword1);
        parameters.put("keyword2", keyword2);
        parameters.put("keyword3", keyword3);
        parameters.put("remark", "请点击查看详情!");
    }

    public static String generateRomdom(int numLen) {
        StringBuffer sb = new StringBuffer();
        String str = "0123456789";
        Random r = new Random();

        for(int i = 0; i < numLen; ++i) {
            int num = r.nextInt(str.length());
            sb.append(str.charAt(num));
            str = str.replace(str.charAt(num) + "", "");
        }

        return sb.toString();
    }

    public static int getInt(String count) {
        try {
            return Integer.parseInt(count);
        } catch (Exception var2) {
            return 0;
        }
    }

    public static double getDouble(String count) {
        try {
            return Double.parseDouble(count);
        } catch (Exception var2) {
            return 0.0D;
        }
    }

    public static String replaceIdentity(String identity) {
        if (isNotEmpty(identity)) {
            identity = identity.replaceAll("(\\d{3})\\d{11}(\\d{4})", "$1***********$2");
        }

        return identity;
    }


}

