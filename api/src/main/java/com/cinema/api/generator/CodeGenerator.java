package com.cinema.api.generator;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CodeGenerator {
    public static void main(String[] args) {
        List<String> tables = new ArrayList<>();
        tables.add("t_activity");
        tables.add("t_admin");
        tables.add("t_arrangement");
        tables.add("t_cart");
        tables.add("t_daily_work");
        tables.add("t_film");
        tables.add("t_film_evaluate");
        tables.add("t_leaving_message");
        tables.add("t_order");
        tables.add("t_order_exception");
        tables.add("t_poster");
        tables.add("t_registration");
        tables.add("t_role");
        tables.add("t_upload");
        tables.add("t_user");
        tables.add("t_worker");
        tables.add("t_worker_evaluate");

        FastAutoGenerator
                .create("jdbc:mysql://localhost:3306/movie","root","root")
                //全局配置
                .globalConfig( builder -> {
                    builder.author("xhh")  //作者
                            .outputDir(System.getProperty("user.dir")+"\\src\\main\\java")   //输出路径
                            .enableSwagger()  //开启Swagger
                            .commentDate("yyyy-MM-dd")
                            .fileOverride();   //覆盖之前文件
                })
                //包配置
                .packageConfig( builder -> {
                    builder.parent("com.cinema")
                            .moduleName("api")
                            .entity("model.entity")
                            .service("service")
                            .serviceImpl("service.Impl")
                            .controller("controller")
                            .mapper("mapper")
                            .xml("mapper")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml,System.getProperty("user.dir")+"\\src\\main\\resources\\mapper"));   //额外处理xml文件
                })
                //策略配置
                .strategyConfig( builder -> {
                    builder.addInclude(tables)
                            .addTablePrefix("t_")   //过滤表前缀
                            .entityBuilder()        //实体类配置
                                .enableLombok()
                                .enableTableFieldAnnotation()
                            .mapperBuilder()        //Mapper策略配置
                            .superClass(BaseMapper.class)
                                .formatMapperFileName("%sMapper")
                                .enableMapperAnnotation()
                                .formatXmlFileName("%sMapper")
                            .serviceBuilder()       //service策略配置
                                .formatServiceFileName("%sService")
                                .formatServiceImplFileName("%sServiceImpl")
                            .controllerBuilder()    //controller策略配置
                                .formatFileName("%sController")
                                .enableRestStyle(); //开启RestController
                })
                .templateEngine(new FreemarkerTemplateEngine())   //使用模板引擎
                .execute();
    }
}
