import service from "@/utils/request";

export function Login(data) {
  return service({
    url: '/user/login',
    method: 'post',
    data: data
  })
}

export function Register(data){
  return service({
    url: '/user/register',
    method: 'post',
    data: data
  })
}

export function findById(id) {
  return service({
    url: '/user/' + id,
    method: 'get'
  })
}

export function uploadAvatar(data) {
  return service({
    url: "/upload",
    method: 'post',
    data: data,
  })
}

export function updateUser(data) {
  return service({
    url: '/user',
    method: 'put',
    data: data
  })
}

//验证
export function verifyTicker(ticket, randstr, userIp) {
  const data = {
    ticket,
    randstr,
    userIp
  }
  return service({
    url: '/user/verifyTicker',
    method: 'post',
    data: data
  })
}
