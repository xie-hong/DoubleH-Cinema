import request from "axios"
import config from "../config"
import {Notification} from 'element-ui'

// 创建axios实例
const token = localStorage.getItem("token")

const service = request.create({
  baseURL: config.API_URL,
  // 链接超时便不会发送（报错）
  // timeout: 10000,
  // 头部可设置访问类型等
  headers: {'Authorization': token}
});

// response 响应拦截器
service.interceptors.response.use(
  // 取消token限制，用户不需要登录也可访问相关页面
  response => {
      const res = response.data;
        if (res.success === true) {
          if (res.msg !== null) {
            // 消息通知
            // Notification.success({
            //   title: 'Success ',
            //   message: res.msg,
            //   type: 'success',
            //   duration: 600
            // });
          }
        } else {
          Notification.error({
            title: '错误提示: ' + res.code,
            message: res.msg,
            duration: 3000
          });
        }
    return res
  },
  error => {
      console.log('err' + error);
      return Promise.reject(error)
  }
);

export default service
